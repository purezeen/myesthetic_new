<?php get_header(); ?>



<?php 
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
$curauth_id = $curauth->ID;
?>

<?php
// Define user ID
// Replace NULL with ID of user to be queried
$user_id = $curauth_id;

// Example: Get ID of current user
// $user_id = get_current_user_id();

// Define prefixed user ID
$user_acf_prefix = 'user_';
$user_id_prefixed = $user_acf_prefix . $user_id;
?>

<?php 
$curauth_name = $curauth->display_name;
$curauth_job_title = get_field( 'job_title', $user_id_prefixed );


$curauth_desc = get_field( 'description', $user_id_prefixed );
$user_id = get_the_author_meta( 'ID' );
$user_avatar = get_avatar( $user_id, 610,  $args = array('class' => 'mobile' )  ); ?>

<?php $string_id = icl_get_string_id( $curauth_name, $curauth_job_title, $curauth_desc  ); ?>
<?php icl_translate('wpml_custom', 'wpml_custom_author_desc_' . $curauth_job_title ); ?>
<?php $relationship_category_with_treatment_term = get_field( 'relationship_category_with_treatment', $user_id_prefixed ); ?>
<?php if ( $relationship_category_with_treatment_term ): ?>
   <?php $relationship_category_term_id = $relationship_category_with_treatment_term->term_id; ?>
<?php endif; ?>

<!-- Page header  -->
<section class="container header blog-header team-single">

   <div class="header-image">

      <?php $vertical_image = get_field( 'vertical_image', $user_id_prefixed ); ?>
      <?php if ( $vertical_image ) { ?>
         <img class="desktop" src="<?php echo $vertical_image['sizes']['image_tablet']; ?>" alt="<?php echo $vertical_image['alt']; ?>" />
      <?php } ?> 
      
      <div class="mobile">
         <?php echo $user_avatar; ?>
      </div>

   </div>

   <div class="header-content">
      <h1><?php echo $curauth_name; ?></h1>
      <span class="team-single__tagline"><?php echo $curauth_job_title; ?></span>
      <div class="product-rating">
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <path id="svgStar" fill="currentColor"
               d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z">
            </path>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
      </div>
      <?php echo $curauth_desc; ?>
   </div>

</section>

<!-- Treatments list  -->
<?php
$args = array(
   'post_type' => 'treatment',
   'posts_per_page' => -1,
   'post_status' => 'publish',  
   'tax_query' => array(
      array(
          'taxonomy' => 'associate',
          'terms' => $relationship_category_term_id,
          'field' => 'term_id',
      )
  ),
);

// The Query
$the_query = new WP_Query( $args );

if ($the_query->have_posts()) : ?>
   <section class="container section pl-sm-0 pr-sm-0 treatments">
      <div class="section-title">
         <h2><?php _e( 'Treatments', 'myesthetic' ); ?></h2>
         <h3 class="section-subtitle"><?php echo _e('By', 'myesthetic'); ?> <?php echo $curauth_name; ?></h3>
      </div>
      <div class="column-3">

         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

         <div class="card">
            <?php get_template_part('template-parts/loop', 'treatment'); ?>
         </div>
         <?php endwhile; ?>
      </div>
   </section>
<?php endif; 
wp_reset_postdata();?>

<!-- Blog posts  -->
<?php $args = array(
   'post_type' => 'post',
   'posts_per_page' => 6,
   'post_status' => 'publish'
   // 'author' => $curauth_id
 );
?>

<?php $the_query = new WP_Query( $args ); ?>

<?php if ($the_query->have_posts()) :  ?>
   <section class="benefit benefit-orange benefit-blog clearfix">
      <div class="benefit-title">
         <h2><?php echo _e('Blog posts', 'myesthetic'); ?></h2>
      </div>
      <div class="slick-benefit-wrap">
         <div class="slick-benefit">
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <?php $post_author =  get_the_author_meta('first_name', $the_query ->post_author); ?>
               <div>
                  <div class="benefit-box">
                     <a href="<?php the_permalink(); ?>" class="blog-card">

                     <?php $blog_header_image = get_field( 'blog_header_image' ); ?>
                        <?php if ( $blog_header_image ) { ?>

                           <div class="blog-img cover" style="background-image: url('<?php echo $blog_header_image['sizes']['medium']; ?>')">

                           </div>
                           
                           <?php } else if(has_post_thumbnail()) { ?>

                           <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'image_tablet' ); ?>

                           <div class="blog-img cover" style="background-image: url('<?php echo $backgroundImg[0]; ?>')"></div>
                        
                        <?php } ?>

                        <div class="blog-content">
                           <span class="blog-date"><?php the_time('d F Y'); ?><span class="separator">|</span>
                           <?php if(!empty($post_author)){
                               echo _e('by', 'myesthetic'); echo " "; echo $post_author;
                           }?></span></span>

                           <h2><?php the_title(); ?></h2>
                           <?php the_excerpt(); ?>
                           <span class="btn btn-link btn-link-arrow"><?php echo _e('Read more', 'myesthetic'); ?></span>
                        </div>
                     </a>
                  </div>
               </div>
            <?php endwhile; ?>
         </div>
      </div>
   </section>
<?php endif; ?>

<?php get_footer(); ?>


