<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

 /* Template Name: Blog */

get_header(); ?>


<!-- Blog header  -->
<?php
    // WP_Query arguments
  $args = array(
      'posts_per_page' => 1
  );

  // The Query
  $the_query = new WP_Query( $args );

  if ($the_query->have_posts()) : ?>
   <section class="container header blog-header">
      <?php while($the_query->have_posts()) : $the_query->the_post();  ?>

      <?php $author = get_the_author_meta('ID'); ?>

       <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
      <?php $backgroundImg_mobile = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
      <?php $backgroundImg_tablet = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium-large' ); ?>

      <?php endif; ?>

        <style>
         .blog-header .header-image {
            background-image: url(<?php echo $backgroundImg_mobile[0]; ?>);
            min-height: 140px;
            height: 140px;
         }

         @media only screen and (min-width : 600px) {
            .blog-header .header-image {
               background-image: url(<?php echo $backgroundImg_tablet[0]; ?>);
               /*min-height: auto;*/
            }
         }

          @media only screen and (min-width : 768px) {
            .blog-header .header-image {
               max-height: auto;
               min-height: auto;
               height: 100%;
            }
         }

      </style>


      <div class="header-image cover" rel="preload" data-skip-lazy="" >
      </div>

      <a href="<?php the_permalink(); ?>" class="header-content">
         <span class="blog-date"><?php the_time('d F Y'); ?><span class="separator">|</span> by <?php echo get_the_author_meta('first_name', $author); ?></span>
         <h1><?php the_title(); ?></h1>
         <?php the_excerpt(); ?>
         <span class="btn btn-full btn-green btn-arrow"><?php _e( 'Read more', 'myesthetic' ); ?></span>
      </a>

      <?php endwhile; ?>
      
      <?php wp_link_pages(); ?> 
      
      <?php 
      wp_reset_postdata();
      ?>
   </section>
   <?php
  endif; 
  ?>

<!-- Blog post list  -->

<?php
// WP_Query arguments

$current_page = get_query_var('paged');
$current_page = max( 1, $current_page );
$per_page = 4;

$offset_start = 1;
$offset = ( $current_page - 1 ) * $per_page + $offset_start;

$args = array(
   'posts_per_page' => $per_page,
   'paged' => $current_page,
   'offset' => $offset
   );

  // The Query
  $post_query = new WP_Query( $args );

  if ($post_query->have_posts()) : ?>
   <?php $count = 0; ?>
   <section class="blog-list container">

   <?php while($post_query->have_posts()) : $post_query->the_post();  ?>
   <?php $count++; ?>
   
   <?php $author = get_the_author_meta('ID'); ?>

   <?php 
   // add cta contact after third post 
   if($count == 3 ) {
      ?>
      <div class="cta section grid-item">
      <?php get_template_part('template-parts/cta', 'contact')?>
      </div>
   <?php
   }
   ?>

   <a href="<?php the_permalink(); ?>" class="blog-card">

      <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
         <?php $backgroundImg_mobile = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
         <?php $backgroundImg_tablet = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>
      <?php endif; ?>

       <style>
         .post-<?php the_ID(); ?>  {
            background-image: url(<?php echo $backgroundImg_mobile[0]; ?>);
         }

         @media only screen and (min-width : 600px) {
            .post-<?php the_ID(); ?>  {
               background-image: url(<?php echo $backgroundImg_tablet[0]; ?>);
            }
         }
      </style>

      <div class="blog-img cover post-<?php the_ID();?>" >
      </div>
      <div class="blog-content">
         <span class="blog-date"><?php the_time('d F Y'); ?><span class="separator">|</span> by <?php echo get_the_author_meta('first_name', $author); ?></span>

         <h2><?php the_title(); ?></h2>

         <?php the_excerpt(); ?>

         <span class="btn btn-link btn-link-arrow"><?php _e( 'Read more', 'myesthetic' ); ?></span>
      </div>
   </a>

   <?php endwhile; ?>

   <!-- Pagination  -->
   <?php
   echo "<div class='pagination'><div>" . paginate_links(array(
      'total' => $post_query->max_num_pages,
      'prev_text' => '<svg xmlns="http://www.w3.org/2000/svg" width="31" height="21" viewBox="0 0 31 21" version="1.1"><title>Path Copy</title><path d="M.19 10.964l9.687 9.844a.638.638 0 0 0 .913 0 .663.663 0 0 0 0-.928l-8.585-8.724h28.15A.65.65 0 0 0 31 10.5a.65.65 0 0 0-.646-.656H2.204L10.79 1.12a.663.663 0 0 0 0-.928.639.639 0 0 0-.913 0L.189 10.036a.664.664 0 0 0 0 .928z" fill="#323231" fill-rule="nonzero" stroke="none" stroke-width="1" opacity=".5"/></svg>', // __('<img src="'.get_bloginfo('template_url').'/img/pagination-arrow-left.svg" alt="<" />'),
      'next_text' => '<svg xmlns="http://www.w3.org/2000/svg" width="31" height="21" viewBox="0 0 31 21" version="1.1"><title>Path</title><path d="M30.81 10.036L21.124.192a.638.638 0 0 0-.913 0 .663.663 0 0 0 0 .928l8.585 8.724H.645A.65.65 0 0 0 0 10.5a.65.65 0 0 0 .646.656h28.15L20.21 19.88a.663.663 0 0 0 0 .928.639.639 0 0 0 .913 0l9.688-9.844a.664.664 0 0 0 0-.928z" fill="#323231" fill-rule="nonzero" stroke="none" stroke-width="1" opacity=".5"/></svg>', // __('<img src="'.get_bloginfo('template_url').'/img/pagination-arrow-right.svg" alt=">" />'),
   )) . "</div></div>";
   ?>

   <?php 
      wp_reset_postdata();
      ?>
   </section>
<?php
  endif; 
  ?>

<?php
get_footer();
