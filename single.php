<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

<?php $blog_header_image = get_field( 'blog_header_image' ); ?>
<?php if ( $blog_header_image ) { ?>

    <section class="single-blog-header container">
    <img src="<?php echo $blog_header_image['sizes']['large']; ?>" srcset="
        <?php echo $blog_header_image['sizes']['medium']; ?> 490w, 
        <?php echo $blog_header_image['sizes']['image_tablet']; ?> 800w, 
        <?php echo $blog_header_image['sizes']['large']; ?> 1265w" sizes="(max-width: 1265px) 1265px, 100vw"
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $blog_header_image['alt']; ?>" rel="preload"
        data-skip-lazy="" />
    </section>
<?php } else if(has_post_thumbnail()) {
  
    $thumb_id = get_post_thumbnail_id(); ?>
    <section class="single-blog-header container">

        <img src="<?php 
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', false);
        echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
        echo $thumb_url_array_medium[0]; ?> 490w, 
        <?php $thumb_url_array_tablet = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
        echo $thumb_url_array_tablet[0]; ?> 800w, 
        <?php echo $thumb_url; ?> 1265w " sizes="(max-width: 1265px) 1265px, 100vw"
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>" rel="preload" data-skip-lazy="" />
    </section>
<?php } ?>

<main class="container">

   <div class="single-blog">
      <div class="single-blog-content">

         <?php $author = get_the_author_meta('ID'); ?>

         <?php
            // Example: Get ID of current user
            // $user_id = get_current_user_id();

            // Define prefixed user ID
            $user_acf_prefix = 'user_';
            $user_id_prefixed = $user_acf_prefix . $author;
            ?>

         <span class="blog-date"><?php the_time('d F Y'); ?><span class="separator">|</span> by
            <?php echo get_the_author_meta('first_name', $author); ?></span>
         <h1 class="single-blog-title"><?php the_title(); ?></h1>

         <hr>

         <div class="single-blog-intro">

            <?php the_content(); // Dynamic Content ?>
            
            <?php get_template_part('template-parts/flexible', 'content');?>
         </div>

         <?php $author = get_the_author_meta('ID'); ?>
      </div>

      <div class="single-blog-author">
         <div class="blog-author-img cover"
            style="background-image: url(<?php echo get_avatar_url( $author, 100 ); ?>)">
         </div>

         <div class="blog-author-content">
            <div class="h4"><?php echo get_the_author_meta('first_name', $author); ?>
               <?php echo get_the_author_meta('last_name', $author); ?></div>

            <?php $author_acf = "user_" . $author; ?>
            <?php
        // get the user role
        $role = get_field('user_subtitle', $author_acf);
        
        // register on the String Translation
        do_action( 'wpml_register_single_string', 'acf-user_subtitle-' . $author . '', 'acf-user_subtitle-'. $author_acf .'', $role );
        
        // display the original or translated string
        $role = apply_filters( 'wpml_translate_single_string', $role, 'acf-user_subtitle-' . $author . '', 'acf-user_subtitle-' . $author_acf . '' );
        
        echo '<span>' . $role . '</span>';
        ?>
            <p><?php echo get_the_author_meta('description', $author); ?></p>
         </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
</main>

<!-- Cta contact  -->
<div class="container cta cta-contact section marginb-0">
   <?php get_template_part('template-parts/cta', 'contact'); ?>
</div>

<!-- Similar blog post  -->
<section class="similar-blog container section">
   <div class="text-center">
      <h2 class="section-title"><?php _e( 'Similar blog posts', 'myesthetic' ); ?></h2>
   </div>
   <?php
  // WP_Query arguments

  $do_not_duplicate = $post->ID;
  $args = array(
      'post_type' => 'post',
      'post__not_in' => array($do_not_duplicate),
      'orderby' => 'rand',
      'posts_per_page' => 3,
  );

  // The Query
  $the_query = new WP_Query( $args );

  if ($the_query->have_posts()) : ?>

   <div class="similar-blog-list">

      <?php while($the_query->have_posts()) : $the_query->the_post();  ?>

      <a href="<?php the_permalink(); ?>" class="card-blog">
      
      <?php $blog_header_image = get_field( 'blog_header_image' ); ?>
      <?php if ( $blog_header_image ) { ?>
         
         <?php $post_img = $blog_header_image['sizes']['medium']; ?>
      <?php } else if(has_post_thumbnail()) {

      $thumb_id = get_post_thumbnail_id(); ?>
      <?php 
         $post_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); 
         $post_img = $post_img[0];
      ?>
      <?php }else {
         $post_img = "";
      } ?>

      <div class="blog-img cover" style="background-image: url('<?php echo $post_img; ?>')"></div>

         <div class="card-blog__content">
            <span class="blog-date"><?php the_time('d F Y'); ?><span
                  class="separator">|</span><?php _e( 'by', 'myesthetic' ); ?>

               <?php  echo get_the_author_meta('first_name', $the_query ->post_author); ?></span>
            <div class="h5"><?php the_title(); ?></div>
            <p><?php the_excerpt(); ?></p>
            <div class="card-footer">
               <span class="btn btn-link btn-arrow "><?php _e( 'Read more', 'myesthetic' ); ?></span>
            </div>
         </div>
      </a>

      <?php endwhile; ?>

      <?php get_template_part('template-parts/pagination'); ?>

      <?php wp_reset_postdata(); ?>
   </div>

   <?php endif; ?>

   <div class="text-center">
      <a href="<?php _e( '/en/blog', 'myesthetic' ); ?>/"
         class="btn btn-full btn-green btn-arrow section-btn"><?php _e( 'All blog posts', 'myesthetic' ); ?></a>
   </div>
</section>

<?php
get_footer();