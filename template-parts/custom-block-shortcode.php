<?php 
  $args = array(
   'post_type' => 'custom_block',
   'post__in' => array((int)$id['id'])
);

// The Query
$the_query = new WP_Query( $args );
$i =1;

$my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );

if ($the_query->have_posts()) : ?>

   <?php while($the_query->have_posts()) : $the_query->the_post();  ?>
      <?php $post_id = get_the_ID(); ?>
      <?php $custom_block_type = get_field( 'select_the_custom_block_type', $post_id ); ?>

      <?php if($custom_block_type == "best_treatment_table"): ?>

         <!-- Best treatment table  -->
         <?php if ( have_rows( 'best_treatment_table', $post_id ) ) : ?>
            <?php while ( have_rows( 'best_treatment_table', $post_id ) ) : the_row(); ?>

               <div class="treatments-table treatments-table--mobile treatments-table--blog">
                  <h2 class="treatments-table__title"><?php echo get_sub_field( 'section_title', $post_id ); ?></h2>
                  <table>
                     <thead>
                        <tr>
                           <th scope="col"><?php _e( 'Products', 'myesthetic' ); ?></th>
                           <th scope="col"><?php _e( 'Rating', 'myesthetic' ); ?></th>
                           <th scope="col"><?php _e( 'Duration', 'myesthetic' ); ?></th>
                           <th scope="col"><?php _e( 'Price', 'myesthetic' ); ?></th>
                           <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if ( have_rows( 'select_the_treatment', $post_id ) ) : ?>
                        
                           <?php while ( have_rows( 'select_the_treatment' , $post_id) ) : the_row(); ?>
                              <?php $post_object = get_sub_field( 'select_the_treatment', $post_id ); ?>
                              <?php if ( $post_object ): ?>
                                 <?php $post = $post_object; ?>
                                 <?php setup_postdata( $post ); ?> 
                                 <?php $post_object_slug = $post->post_name; ?>
                                 <?php $post_object_id = $post->ID; ?>
                                    <tr>
                                       <th scope="row"><a class="row-title" href="<?php echo get_permalink( $post_object_id ); ?>"><?php echo $post->post_title; ?></a></th>
                                       <td>
                                          <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                                          <?php echo $wpdrating; ?>
                                          <?php else:
                                             if (shortcode_exists('product_rating')) {
                                                do_shortcode('[product_rating]');
                                             }
                                          endif; ?>
                                       </td>
                                       <td class="bigger-text" data-title="Duration">
                                       <?php echo (get_field( 'duration', $post_object_id ))? get_field( 'duration', $post_object_id ) ."<span> MIN</span>" : ""; ?></td>
                                       <td class="bigger-text" data-title="<?php _e( 'Price ', 'myesthetic' ); echo (get_field( 'treatment_add_label_from', $post_object_id ) == 1)? _e( 'from: ', 'myesthetic' ) : ""; ?>">

                                          <?php if(get_field( 'treatment_price', $post_object_id )): ?>
                                             <span class="from-label">
                                             <?php if ( get_field( 'treatment_add_label_from', $post_object_id ) == 1 ) { 
                                                echo _e( 'from', 'myesthetic' );
                                             } ?></span>
                                             <?php the_field( 'treatment_price', $post_object_id ); ?><span>€</span>
                                          <?php endif; ?>
                                       
                                       </td>
                                       </td>
                                       <td><a class="btn btn-full btn-green" href="<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>/" onclick="location='<?php echo $my_home_url; ?>/<?php _e( 'contact', 'myesthetic' ); ?>?treat=<?php echo $post_object_slug; ?>#treatments'; return false;"><?php _e( 'Book now', 'myesthetic' ); ?></a></td>
                                    </tr>
                                 <?php wp_reset_postdata(); ?>
                              <?php endif; ?>
                           <?php endwhile; ?>
            
                        <?php endif; ?>
                        
                     </tbody>
                  </table>

                  <?php $section_button = get_sub_field( 'section_button' ); ?>
                  <?php if ( $section_button ) { ?>
                     <a href="<?php echo $section_button['url']; ?>" target="<?php echo $section_button['target']; ?>" class="btn btn-arrow btn-arrow-green"><?php echo $section_button['title']; ?></a>
                  <?php } ?>

               </div>
            <?php endwhile; ?>
         <?php endif; ?>
        <!-- End best treatment table  -->
      <?php endif; ?>
    
      <?php if($custom_block_type == "best_product_table"): ?>
         <!-- Best product table -->
         <?php if ( have_rows( 'best_product_table', $post_id ) ) : ?>
            <?php while ( have_rows( 'best_product_table', $post_id ) ) : the_row(); ?>
               <?php if ( get_sub_field( 'set_custom_block_as_section' ) == 1 ) { 
                  $section_classes = "container section";
               } else { 
                  $section_classes = "treatments-table--blog";
               } ?>

               <div class="treatments-table treatments-table--mobile <?php echo  $section_classes; ?>">
                  <h2 class="treatments-table__title"><?php the_sub_field( 'section_title' ); ?></h2>
                  <table>
                     <thead>
                        <tr>
                        <th scope="col"><?php _e( 'Products', 'myesthetic' ); ?></th>
                        <th scope="col"><?php _e( 'Rating', 'myesthetic' ); ?></th>
                        <th scope="col"><?php _e( 'Price', 'myesthetic' ); ?></th>
                        <th></th>
                        <th></th>
                        </tr>
                     </thead>
                     <tbody>

                        <?php if ( have_rows( 'select_the_product', $post_id ) ) : ?>
               
                           <?php while ( have_rows( 'select_the_product', $post_id ) ) : the_row(); ?>
                              <?php $post_object = get_sub_field( 'select_the_product', $post_id ); ?>
                              <?php if ( $post_object ): ?>
                                 <?php $post_product = $post_object; ?>
                                 <?php $post_product_id = $post_product->ID; ?>
                                 <?php $post_product_slug = $post_product->post_name; ?>
                                 <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>
                                 <?php setup_postdata( $post_product ); ?> 
                                 <?php global $product; ?>
                                 <?php $symbol = get_woocommerce_currency_symbol(); ?>
                                    <tr>
                                       <th scope="row"><a class="row-title" href="<?php echo get_permalink( $post_product_id ); ?>"><?php echo $post_product->post_title; ?></a></th>
                                       <td>
                                          <div class="product-rating">
                                             <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                                                <?php echo $wpdrating; ?>
                                                <?php else:
                                                   if (shortcode_exists('product_rating')) {
                                                      do_shortcode('[product_rating]');
                                                   }
                                             endif; ?>
                                          </div>
                                       </td>
                                       <td class="bigger-text" data-title="Price"><?php echo $product->get_price(); ?><span><?php echo $symbol; ?></span></td>
                                       <td class="link-more"><a href="<?php echo get_permalink( $post_product_id ); ?>" class="btn"><?php _e('Learn more', 'myesthetic'); ?></a></td>
                                       <td class="add-to-card-animation">
                                          <a class="btn btn-full btn-green woo-single-product add-to-card" href="<?php echo home_url() . '/produkte?add-to-cart=' . $post_product_id; ?>">  
                                          <span><?php _e( 'Order now', 'myesthetic' ); ?></span>
                                          <div class="loading-icon">
                                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                          <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
                                          </svg></div>
                                          <div class="load-finish">
                                          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                             <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
                                                stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
                                          </svg>
                                          </div>
                                         </a>
                                       </td>
                                    </tr>
                                 <?php wp_reset_postdata(); ?>
                              <?php endif; ?>
                           <?php endwhile; ?>

                        <?php endif; ?>
                  
                     </tbody>
                  </table>
               </div>
            <?php endwhile; ?>
         <?php endif; ?>

      <?php endif; ?>

      <?php if($custom_block_type == "top_product"): ?>
         <!-- Top product -->
         <?php if ( have_rows( 'top_product', $post_id ) ) : ?>
            <?php $top_product_counter = 1; ?>

            <?php while ( have_rows( 'top_product', $post_id ) ) : the_row(); ?>
               <div class="top-3">
                  <h2 class="top-3__title"><?php the_sub_field( 'section_title', $post_id ); ?></h2>
                  <?php if ( have_rows( 'select_the_product', $post_id ) ) : ?>
                     <?php while ( have_rows( 'select_the_product', $post_id ) ) : the_row(); ?>
                        <?php $post_object = get_sub_field( 'select_the_product', $post_id ); ?>
                        <?php if ( $post_object ): ?>
                           <?php $top_product = $post_object; ?>
                           <?php setup_postdata( $top_product ); ?> 
                           <?php $top_product_id = $top_product->ID; ?>
                           <?php  $symbol = get_woocommerce_currency_symbol(); ?>
                           <?php global $product; ?>
                           <?php   $woo_product_cat = get_the_terms( $top_product_id, 'product_cat' ); ?>
                           <?php  $cat_product_name = $woo_product_cat[0]->name; ?>
                              <div class="top-3__item">
                                 <span class="top-3__rating-number"><?php echo $top_product_counter; ?></span>
                                 <div class="top-3__img">
                                    <?php if ( has_post_thumbnail($top_product_id) ) {
                                          $featured_img_url = get_the_post_thumbnail_url($top_product_id,'thumbnail');
                                    } ?>
                                    <div class="top-3__img--wrap">
                                       <img src="<?php echo $featured_img_url; ?>" 
                                          alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
                                          width="190"
                                          height="190">
                                    </div>
                                 </div>
                                 <div class="top-3__name">
                                    <span class="top-3__small-caption"><?php echo $cat_product_name; ?></span>
                                    <h3><a href="<?php echo get_permalink($top_product_id); ?>"><?php echo $top_product->post_title; ?></a></h3>
                                 </div>
                                 <div class="top-3__description">
                                    <?php if(get_field( 'best_for:', $top_product_id)): ?>
                                       <span class="top-3__small-caption"><?php _e( 'Best of: ', 'myesthetic' ); ?></span>
                                       <h4><?php the_field( 'best_for:', $top_product_id); ?></h4>
                                    <?php endif; ?>
                                 </div>
                                 <div class="top-3__price">
                                    <span class="top-3__small-caption"><?php _e( 'Price:', 'myesthetic' ); ?></span>
                                    <span class="top-3__price-number"><?php echo $product->get_price(); ?><?php echo $symbol; ?></span>
                                    <div class="top-3__rating">
                                       <div class="product-rating">
                                          <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                                             <?php echo $wpdrating; ?>
                                             <?php else:
                                                if (shortcode_exists('product_rating')) {
                                                   do_shortcode('[product_rating]');
                                                }
                                          endif; ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="top-3__buttons add-to-card-animation">
                                    <a href="<?php echo home_url() . '/produkte?add-to-cart=' . $product->get_id(); ?>"
                                       class="btn btn-full btn-green woo-single-product add-to-card">
                                       <span><?php _e( 'Order now', 'myesthetic' ); ?></span>
                                       <div class="loading-icon">
                                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                          <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
                                          </svg>
                                       </div>
                                       <div class="load-finish">
                                          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                                          <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
                                             stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
                                          </svg>
                                       </div>
                                    </a>

                                    <a href="<?php echo get_permalink($top_product_id); ?>" class="btn"><?php _e( 'Learn more', 'myesthetic' ); ?></a>
                                 </div>
                              </div>
                              
                           <?php wp_reset_postdata(); ?>
                        <?php endif; ?>
                        <?php $top_product_counter++; ?>
                     <?php endwhile; ?>
                  <?php endif; ?>
               </div>
                  
            <?php endwhile; ?>
         <?php endif; ?>
         <!-- End top product  -->
      <?php endif; ?>
     
      <?php if($custom_block_type == "custom_table"): ?>
         <!-- Custom table  -->
         <?php $table = get_field( 'custom_table', $post_id ); 
         $table_caption = $table['caption'];
          if ( $table ) { ?>
            <section class="treatments-table treatments-table--blog treatments-table--custom">
            <?php echo ($table_caption)? "<h2 class='treatments-table__title'>" . $table_caption . "</h2>": ""; ?>
               <div class="table-wrapper">
                  <table>
                     <?php if ( $table['header'] ) { 
                        echo '<thead><tr>'; 
                           echo ''; 
                                 foreach ( $table['header'] as $th ) { 
                                    echo '<th scope="col">'; 
                                       echo $th['c']; 
                                    echo '</th>'; 
                                 } 
                           echo '</tr>'; 
                        echo '</thead>'; 
                     } 
                     echo '<tbody>'; 
                        foreach ( $table['body'] as $tr ) { 
                           echo '<tr>'; 
                              foreach ( $tr as $td ) { 
                                 echo '<td>'.$td['c'].'</td>'; 
                              } 
                           echo '</tr>'; 
                        } 
                     echo '</tbody>'; ?>
                  </table>
               </div>
            </section>
           <?php } ?> 
         <!-- End custom table  -->
      <?php endif; ?>
   
   <?php endwhile; ?>

<?php endif; ?>
<?php wp_reset_postdata(); ?>




  
