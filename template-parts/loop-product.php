<?php global $product; ?>
<a href="<?php the_permalink(); ?>">
   <div>
      <?php
        $woo_product_categories = get_the_terms( get_the_ID(), 'product_cat' );
         foreach($woo_product_categories as $woo_product_cat) {
            if ( $woo_product_cat->parent !== 0 ) {
               $cat_product_name = $woo_product_cat->name;
            }
         }

        $symbol = get_woocommerce_currency_symbol();
      ?>

      <p class="card-product-cat"><?php echo $cat_product_name; ?></p>
      <div class="card-product-title h5"><?php the_title(); ?></div>
   </div>

   <div class="card-product-content">
      <?php if ( has_post_thumbnail() ) {
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumbnail');
        } ?>
      <div class="card-product-img">
         <img src="<?php echo $featured_img_url; ?>" 
            alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
            width="160"
            height="160"
         />
      </div>
      <div class="card-product-price">
         <span><?php _e( 'Price: ', 'myesthetic' ); ?></span>
         <h3><?php echo $product->get_price(); ?><span class="currency"><?php echo $symbol; ?></span></h3>
         <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating]')): ?>
            <?php echo $wpdrating; ?>
         <?php else:
            if (shortcode_exists('product_rating')) {
	            do_shortcode('[product_rating]');
            }
         endif; ?>
      </div>
   </div>
</a>
<div class="card-footer add-to-card-animation">
   <a href="<?php echo get_permalink( $product->get_id() ); ?>"
      class="btn btn-link btn-arrow"><?php _e( 'Learn more', 'myesthetic' ); ?></a>
   <a href="<?php echo home_url() . '/produkte?add-to-cart=' . $product->get_id(); ?>"
      class="btn btn-border woo-single-product add-to-card">

      <span><?php _e( 'Order now', 'myesthetic' ); ?></span>
      <div class="loading-icon">
         <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
         <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
         </svg></div>
         <div class="load-finish">
         <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
               stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
         </svg>
      </div>
   </a>
</div>