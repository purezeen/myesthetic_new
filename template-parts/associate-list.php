<!-- Associte  -->
<?php if ( have_rows( 'associate' ) ) : ?>
   <div class="associate__items">
      <?php while ( have_rows( 'associate' ) ) : the_row(); ?>
         <?php $post_object = get_sub_field( 'select_and_associate' ); ?>
         <?php if ( $post_object ): ?>
            <?php $post = $post_object; ?>
            <?php setup_postdata( $post ); ?> 
               <a href="<?php the_permalink(); ?>" class="associate__item">
                  <div class="associate__profile">
                     <div class="associate__profile--img">
                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                           <?php $image_id = get_post_thumbnail_id($post->ID ); ?>
                           <?php $alt_text = get_post_meta($image_id , '_wp_attachment_image_alt', true);?> 
                           <div class="mobile">
                              <img src="<?php echo the_post_thumbnail_url('thumbnail') ?>" alt="<?php echo $alt_text ;?>">
                           </div>
                        <?php endif; ?>
                     
                        <img class="bg-image" src="<?php echo get_template_directory_uri() ?>/img/group-bg1.png" alt="myesthetic">
                     </div>
                     <div class="associate__profile--content">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_field( 'job_title' ); ?></p>
                        <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                           <?php echo $wpdrating; ?>
                           <?php else:
                              if (shortcode_exists('product_rating')) {
                                 do_shortcode('[product_rating]');
                              }
                        endif; ?>
                     </div>
                  </div>
                  <div class="associate__treatments">
                   <?php the_sub_field( 'description' ); ?>
                  </div>
                  <div class="associate__btn">
                     <button class="btn btn-full btn-green btn-arrow"><?php _e('Learn more', 'myesthetic'); ?></button>
                  </div>
               </a>

            <?php wp_reset_postdata(); ?>
         <?php endif; ?>
        
      <?php endwhile; ?>
   </div>
<?php endif; ?>
