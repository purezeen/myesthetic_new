<?php if (get_field( 'show_promotion', 'option' ) == 1): ?>
   <div class="banner">
      <div class="banner-content">
         <img class="banner-content-shape" src="<?php echo get_template_directory_uri() ?>/img/banner-image-shape.png" width="122" height="116" alt="Myesthetic">
         <div class="banner-content-inner">
            <?php the_field( 'promotion_text', 'option' ); ?>
         </div>
      </div>
      <div class="banner-image">
         <div class="banner-main-image">
            <img class="banner-main-inner-image" src="<?php echo get_template_directory_uri() ?>/img/banner-image.png" width="386" height="334" alt="Myesthetic">
            <img class="banner-shape-right" src="<?php echo get_template_directory_uri() ?>/img/banner-shaperight.png" width="293" height="182" alt="Myesthetic">
            <img class="banner-shape-left" src="<?php echo get_template_directory_uri() ?>/img/banner-shapeleft.png" width="339" height="162" alt="Myesthetic">
         </div>
      </div>
   </div>
<?php endif; ?>
