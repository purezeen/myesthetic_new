<?php if ( have_rows( 'flexible_content_section' ) ): ?>
   <section class="container section editor">aaaaaaa

	<?php while ( have_rows( 'flexible_content_section' ) ) : the_row(); ?>
	 <!-- Text editor  -->
    <?php if ( get_row_layout() == 'text_editor' ) : ?>
      <?php the_sub_field( 'text_editor' ); ?>

      <!-- Two columns  -->
      <?php elseif ( get_row_layout() == 'two_columns' ) : ?>
         <div class="coupon-column">
            <div class="coupon-title">
               <?php the_sub_field( 'left_column' ); ?>
            </div>
            <div class="coupon-content">
               <?php the_sub_field( 'right_column' ); ?>
            </div>
      </div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>
</section>