<?php 

  $args = array(
   'post_type' => 'product',
   'post__in' => array((int)$id['id'])
);
// The Query
$the_query = new WP_Query( $args );
$my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );

if ($the_query->have_posts()) : ?>
  
   <?php while($the_query->have_posts()) : $the_query->the_post();  ?>
   <?php global $product; ?>
   <?php 
	   $woo_product_cat = get_the_terms( get_the_ID(), 'product_cat' );
      $firstCategory = $woo_product_cat[0]->name; ?>

      <div class="single-product-header promo-product">

         <div class="single-product-title">
            <h2><?php the_title(); ?></h2>
         </div>

         <div class="single-product-text">
            <?php the_excerpt(); ?>
         </div>

         <div class="single-product-content">

            <div class="single-product-price">

               <div class="h6 single-product-brand"><?php _e( 'Brand:', 'myesthetic' ); ?><strong><?php echo $firstCategory; ?></strong></div>

               <div class="h6"><?php _e( 'Rating:', 'myesthetic' ); ?>
                  <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                     <?php echo $wpdrating; ?>
                  <?php else:
                     if (shortcode_exists('product_rating')) {
                        do_shortcode('[product_rating]');
                     }
                  endif; ?>
               </div>
               <div class="h6"><?php _e( 'Price:', 'myesthetic' ); ?><strong><?php echo $product->get_price(); ?><span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span></strong></div>

            </div>

            <?php $hero_image = get_field( 'hero_image' ); ?>
            <?php if ( $hero_image ) { ?>
            <div class="single-product-image">
               <img src="<?php echo $hero_image['sizes']['medium']; ?>" 
                  alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" 
                  width="<?php echo $hero_image['sizes']['medium-width']; ?>"
                     height="<?php echo $hero_image['sizes']['medium-height']; ?>"
               />
               </div>

               <?php } else if(has_post_thumbnail()){
                     $thumb_id = get_post_thumbnail_id();
                     ?>
                     <div class="single-product-image">
                     <img src="<?php 
                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', false);
                        echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
                        echo $thumb_url_array_medium[0]; ?> 490w" sizes="(max-width: 1265px) 1265px, 100vw"
                        width="<?php echo $thumb_url_array[1]; ?>"
                        height="<?php echo $thumb_url_array[2]; ?>"
                     />
                     </div>
                     <?php
               } ?>
            </div>
 
            <div class="single-product-btn add-to-card-animation">

               <a href="<?php echo home_url() . '/produkte?add-to-cart=' . $product->get_id(); ?>"
                  class="btn btn-green btn-full woo-single-product add-to-card">

                  <span><?php _e( 'Order your product now', 'myesthetic' ); ?></span>
                  <div class="loading-icon">
                     <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                     <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
                     </svg>
                  </div>
                  <div class="load-finish">
                     <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                     <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
                        stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
                     </svg>
                  </div>
               </a>

               <a href="<?php echo get_permalink( $product->get_id() ); ?>" class="btn btn-link btn-arrow btn-arrow-green"><?php _e( 'Learn more', 'myesthetic' ); ?></a>
            </div>

      </div>

   <?php endwhile; ?>
<?php endif; ?>

<?php wp_reset_postdata(); ?>




  
