  <!-- Flexible content  -->
  <?php if ( have_rows( 'flexible_content' ) ): ?>
  <?php while ( have_rows( 'flexible_content' ) ) : the_row(); ?>
  <?php if ( get_row_layout() == 'faq' ) : ?>

   <!-- Faq  -->
   <div class="faq flexible-content-wrap">
      <div class="faq-toggle">
         <button class="arrow-down"></button>
         <h3><?php the_sub_field( 'faq_question' ); ?></h3>
         <div class="toggle-content">
            <?php the_sub_field( 'faq_answer' ); ?>
         </div>
      </div>
   </div>

  <!-- Text editor  -->
  <?php elseif ( get_row_layout() == 'text_editor' ) : ?>
   <div class="flexible-content-wrap">
      <?php the_sub_field( 'text_editor' ); ?>
   </div>
  
  <!-- Two columns  -->
  <?php elseif ( get_row_layout() == 'two_columns' ) : ?>
      <div class="coupon-column flexible-content-wrap">
         <div class="coupon-title">
            <?php the_sub_field( 'left_column' ); ?>
         </div>
         <div class="coupon-content">
            <?php the_sub_field( 'right_column' ); ?>
         </div>
      </div>
  
   <?php endif; ?>
  <?php endwhile; ?>
  <?php endif; ?>