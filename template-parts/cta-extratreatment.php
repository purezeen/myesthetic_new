<?php if ( have_rows( 'cta_extra_treatment', 'option' ) ) : ?>
   <?php while ( have_rows( 'cta_extra_treatment', 'option' ) ) : the_row(); ?>
   
      <div class="cta-content">
         <?php the_sub_field( 'cta_extratreatment_content' ); ?>
      </div>

      <a href="<?php the_sub_field( 'button_extratreatment_url' ); ?>" class="btn btn-full btn-orange btn-arrow"><?php the_sub_field( 'button_extratreatment_text' ); ?></a>
		
	<?php endwhile; ?>
<?php endif; ?>
