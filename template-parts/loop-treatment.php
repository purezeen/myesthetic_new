<!-- Treatment box  -->
<a href="<?php the_permalink(); ?>">

    <?php
      $categories = get_the_category();
      foreach($categories as $category) {
          $has_parent = $category->parent;
          if($has_parent) {
            $category_id = $category->term_id;
          }
      } ?>

    <?php
    // Define taxonomy prefix
    // Replace NULL with the name of the taxonomy eg 'category'
    $taxonomy_prefix = 'category';

    // Define term ID
    // Replace NULL with ID of term to be queried eg '123' 
    $term_id = $category_id;

    // Define prefixed term ID
    $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;
    ?>

    <?php $category_image = get_field( 'category_image', $term_id_prefixed ); ?>
    <?php if ( $category_image ) { ?>
    <div class="card-icon">
        <img src="<?php echo $category_image['sizes']['thumbnail']; ?>" alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $category_image['alt']; ?>" width="70" height="70" />
    </div>
    <?php } ?>

    <div class="h5"><?php the_title(); ?></div>
    <?php the_excerpt() ?>
</a>

<div class="card-footer">
    <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
        <?php echo $wpdrating; ?>
    <?php else:
        if (shortcode_exists('product_rating')) {
        do_shortcode('[product_rating]');
        }
    endif; ?>
    <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>
    <?php $post_slug = $post->post_name; ?>
    <a href="<?php the_permalink(); ?>" class="btn btn-link btn-arrow"><?php _e( 'Learn more', 'myesthetic' ); ?></a>
    <a href="<?php echo $my_home_url; ?>/<?php _e( 'contact', 'myesthetic' ); ?>/"
        onclick="location='<?php echo $my_home_url; ?>/<?php _e( 'contact', 'myesthetic' ); ?>?treat=<?php echo $post_slug; ?>#treatments'; return false;"
        class="btn btn-border"><?php _e( 'Book this treament', 'myesthetic' ); ?></a>
</div>