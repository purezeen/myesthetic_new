<?php if ( have_rows( 'contact_cta', 'option' ) ) : ?>
   <?php while ( have_rows( 'contact_cta', 'option' ) ) : the_row(); ?>
   
      <div class="cta-content">
         <?php the_sub_field( 'cta_content' ); ?>
      </div>

  <a href="<?php the_sub_field( 'button_url' ); ?>" class="btn btn-full btn-orange btn-arrow"><?php the_sub_field( 'button_text' ); ?></a>
		
	<?php endwhile; ?>
<?php endif; ?>
