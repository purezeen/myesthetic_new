<?php 
  $args = array(
   'post_type' => 'treatment',
   'post__in' => array((int)$id['id'])
);

// The Query
$the_query = new WP_Query( $args );
$my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );

if ($the_query->have_posts()) : ?>
   <?php global $post; ?>
   <?php while($the_query->have_posts()) : $the_query->the_post();  ?>
      <?php $team_post_categories = get_the_category(); ?>
      <?php $array_cat_slug = array(); ?>
      <?php foreach($team_post_categories as $team_post_category) {
         $array_cat_slug[] = $team_post_category->slug;
      } ?>
      <?php  if (in_array("extratreatment", $array_cat_slug)): ?>
         <?php $extra_treatment = "promo-treatment--orange"; ?>
      <?php else: ?>
         <?php $extra_treatment = "promo-treatment--green"; ?>
      <?php endif; ?>
      
      <section class="single-treatment-header promo-treatment <?php echo $extra_treatment; ?>">

         <?php 
         $post_slug = $post->post_name; ?>
         <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>

         <?php $hero_treatment_image = get_field( 'hero_treatment_image', $post->ID ); ?>
         <?php if ( $hero_treatment_image ) { ?>

            <div class="header-image">
               <img src="<?php echo $hero_treatment_image['sizes']['large']; ?>" srcset="
               <?php echo $hero_treatment_image['sizes']['medium']; ?> 490w, 
               <?php echo $hero_treatment_image['sizes']['image_tablet']; ?> 800w, 
               <?php echo $hero_treatment_image['sizes']['large']; ?> 1265w" sizes="(max-width: 1265px) 1265px, 100vw"
                  alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_treatment_image['alt']; ?>" rel="preload"
                  data-skip-lazy="" />
            </div>

         <?php } else if(has_post_thumbnail( $post->ID)) {
         
            $thumb_id = get_post_thumbnail_id( $post->ID); ?>
            <div class="header-image">

               <img src="<?php 
                  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', false);
                  echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
                  echo $thumb_url_array_medium[0]; ?> 490w, 
                  <?php $thumb_url_array_tablet = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
                  echo $thumb_url_array_tablet[0]; ?> 800w, 
                  <?php echo $thumb_url; ?> 1265w " sizes="(max-width: 1265px) 1265px, 100vw"
                  alt="<?php _e('my-esthetic', 'myesthetic'); ?>" rel="preload" data-skip-lazy="" />
            </div>

         <?php } ?>

            <div class="header-content">
               <div class="card-icon">
                  <img src="<?php echo get_template_directory_uri() ?>/img/full-green-icon.png" alt="" width="100px"
                     height="100px">
               </div>

               <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

               <?php the_field('hero_treatment_content', $post->ID); ?>

               <div class="promo-treatment__info">
                  <?php _e( 'Rating: ', 'myesthetic' ); ?>
                     <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                     <?php echo $wpdrating; ?>
                     <?php else:
                        if (shortcode_exists('product_rating')) {
                           do_shortcode('[product_rating]');
                        }
                     endif; ?>
               </div>

               <?php if(get_field( 'price' )): ?>
                  <div class="promo-treatment__info promo-treatment__info--center">
                  <?php _e( 'Price: ', 'myesthetic' ); ?><strong><?php the_field( 'price' ); ?><span class="currency">€</span></strong>
                  </div>
               <?php endif; ?>


               <?php if(get_field( 'treatment_price', $post->ID )): ?>
                  <div class="promo-treatment__info promo-treatment__info--center">

                     <?php _e( 'Price: ', 'myesthetic' ); ?><strong>
                     <?php if ( get_field( 'treatment_add_label_from', $post->ID ) == 1 ) { 
                        echo _e( 'from', 'myesthetic' );
                     } ?>
                      <?php the_field( 'treatment_price', $post->ID ); ?>€</strong>
                     
                  </div>
                  <?php endif; ?>
               <div class="promo-treatment__buttons">
                  <a class="btn btn-full btn-green" href="<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>/" onclick="location='<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>?treat=<?php echo $post_slug; ?>#treatments'; return false;" class="btn btn-full btn-arrow btn-green"><?php _e( 'Book appointment', 'myesthetic' ); ?></a>
                  <a href="<?php the_permalink(); ?>" class="btn btn-link btn-arrow btn-arrow-green"><?php _e( 'Learn more', 'myesthetic' ); ?></a>
               </div>
            </div>

         <?php wp_reset_postdata(); ?>

      </section>

   <?php endwhile; ?>
<?php endif; ?>




  
