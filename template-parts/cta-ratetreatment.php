<div class="cta-content">
   <?php if ( have_rows( 'cta_rate_treatment', 'option' ) ) : ?>
   	<?php while ( have_rows( 'cta_rate_treatment', 'option' ) ) : the_row(); ?>
   		<?php the_sub_field( 'cta_treatment_content' ); ?>
   	<?php endwhile; ?>
   <?php endif; ?>

   <div class="cta-stars">
   <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
      <?php echo $wpdrating; ?>
   <?php else:
      if (shortcode_exists('product_rating')) {
         do_shortcode('[product_rating]');
      }
   endif; ?>
   </div>
</div>

<div class="open-modal">
   <div class="btn btn-full btn-orange btn-arrow section-btn"><?php _e( 'Rate', 'myesthetic' ); ?></div>
</div>


