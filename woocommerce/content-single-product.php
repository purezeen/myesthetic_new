<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php 
 if ( get_field( 'show_promotion', 'option' ) == 1 ) { 
    $header_promo = "header-promo-single";
  }
  ?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<!-- Product Header -->
	<section class="single-product-header container <?php echo $header_promo; ?>">
	   <?php 
	   $woo_product_cat = get_the_terms( get_the_ID(), 'product_cat' );
      $firstCategory = $woo_product_cat[0]->name;
			?>
	   <div class="single-product-title">
	      <span class="single-product-cat"><?php echo $firstCategory; ?></span>
	      <h1><?php the_title(); ?></h1>
	   </div>

	   <div class="single-product-content">
	      <div class="single-product-price">
	         <h6><?php _e( 'Price:', 'myesthetic' ); ?> <strong><?php echo $product->get_price(); ?><span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span></strong></h6>
				<?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
                  <?php echo $wpdrating; ?>
               <?php else:
                  if (shortcode_exists('product_rating')) {
                     do_shortcode('[product_rating]');
                  }
               endif; ?>
			
	      </div>

	      <?php $hero_image = get_field( 'hero_image' ); ?>
	      <?php if ( $hero_image ) { ?>
	      <div class="single-product-image">
			<img src="<?php echo $hero_image['sizes']['medium']; ?>" 
				alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" 
				width="<?php echo $hero_image['sizes']['medium-width']; ?>"
         		height="<?php echo $hero_image['sizes']['medium-height']; ?>"
			/>
	      </div>

	      <?php } else if(has_post_thumbnail()){
			       $thumb_id = get_post_thumbnail_id();
			      ?>
			      <div class="single-product-image">
			        <img src="<?php 
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', false);
						echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
						echo $thumb_url_array_medium[0]; ?> 490w" sizes="(max-width: 1265px) 1265px, 100vw"
						width="<?php echo $thumb_url_array[1]; ?>"
            			height="<?php echo $thumb_url_array[2]; ?>"
					/>
			      </div>
			      <?php
			  } ?>
	   </div>

	   <div class="single-product-btn">
	   	<?php 
				// To change add to cart text on single product page
				?>
				<div class="single-product-number">
				<?php
				add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
				?>
				
			<?php
				function woocommerce_custom_single_add_to_cart_text() {
				    return __( 'Order your product now', 'myesthetic' ); 
				}
			 ?>

			<?php do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' ); ?>
			</div>
		</div>

		<?php if ( get_field( 'show_promotion', 'option' ) == 1 ) { ?>
		<div class="header-promo-wrap">
			<div class="header-promo-single-banner">
				<?php the_field( 'promotion_text', 'option' ); ?>
			</div>
		</div>
		<?php
		} ?>
	</section>

	<section class="layout-sidebar container">
	   <main class="single-treatment-container">
	      <div class="single-treatment-content editor">

	         <?php the_content(); ?>

	         <!-- Flexible content  -->
	         <?php if ( have_rows( 'flexible_content' ) ): ?>
	         <?php while ( have_rows( 'flexible_content' ) ) : the_row(); ?>
	         <?php if ( get_row_layout() == 'faq' ) : ?>

	         <!-- Faq  -->
	         <div class="faq">
	            <div class="faq-toggle">
	               <button class="arrow-down"></button>
	               <h6><?php the_sub_field( 'faq_question' ); ?></h6>
	               <div class="toggle-content">
	                  <?php the_sub_field( 'faq_answer' ); ?>
	               </div>
	            </div>
	         </div>

	         <!-- Text editor  -->
	         <?php elseif ( get_row_layout() == 'text_editor' ) : ?>
	         <?php the_sub_field( 'text_editor' ); ?>
	         <?php endif; ?>
	         <?php endwhile; ?>
	         <?php else: ?>
	         <?php // no layouts found ?>
	         <?php endif; ?>

	      </div>
	   </main>

	   <!-- Sidebar product  -->
	   <aside class="sidebar">
	      <div class="sidebar-box">
	         <div class="sidebar-content">
	            <h2><strong><?php _e( 'Product info', 'myesthetic' ); ?></strong></h2>
	            <?php
	            the_field( 'treatment_single' );?>
	         </div>
	         <div class="sidebar-content">
		            <div class="h5"><?php _e( 'Price:', 'myesthetic' ); ?> <strong><?php echo $product->get_price(); ?> EUR</strong></div>
	         </div>
	         <div class="sidebar-footer ">
	            <span class="sidebar-open"><?php _e( 'Product info', 'myesthetic' ); ?><span class="arrow-down"></span></span>
					<a href="<?php echo home_url() . '/produkte?add-to-cart=' . $product->get_id(); ?>" class="btn btn-full btn-green woo-single-product"><?php _e( 'Order now', 'myesthetic' ); ?>
				
					</a>
	         </div>
	      </div>
	   </aside>
	</section>

	<!-- Add flexible content section  -->
	<?php get_template_part('template-parts/flexible', 'contentsection');?>

	<?php $slider_images_images = get_field( 'slider_images' ); ?>
	<?php if ( $slider_images_images ) :  ?>
	<section class="product-slider container">
	   <div class="slick-product">

	      <?php foreach ( $slider_images_images as $slider_images_image ): ?>

	      <div>
	         <div class="slider-product-wrap">
					<a href="<?php echo $slider_images_image['sizes']['large']; ?>" data-rel="lightcase:myCollection">
						<img src="<?php echo $slider_images_image['sizes']['large']; ?>" 
							alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $slider_images_image['alt'];?>"
							width="<?php echo $slider_images_image['sizes']['large-width']; ?>"
        					height="<?php echo $slider_images_image['sizes']['large-height']; ?>"
						/>
					</a>
	         </div>
	      </div>
	      <?php endforeach; ?>
	   </div>
	</section>

	<?php endif; ?>

	<div class="cta cta-rate section container">
	   <?php get_template_part('template-parts/cta', 'rate') ?>
	</div>

	<div class="cta cta-contact section container">
	   <?php get_template_part('template-parts/cta', 'contact');?>
	</div>


   <?php
    // WP_Query arguments
    $args = array(
		'category__in' => wp_get_post_categories( get_queried_object_id() ),
      'post_type' => 'product',
      'post__not_in' => array($post->ID),
      'posts_per_page' => 3,
		'orderby' => 'rand',
		'post__not_in' => array( 5445, 5446, 5697, 5590 )
    );

    // The Query
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) : ?>

		<section class="similar-product container section">
		   <div class="text-center">
		      <h2 class="section-title"><?php _e( 'Similar products', 'myesthetic' ); ?></h2>
		   </div>
		 	 <div class="product-list column-3">

	      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
	      <div class="card similar-treatment-card">
	         <?php get_template_part('template-parts/loop', 'product')?>
	      </div>
	      <?php endwhile; ?>

   		</div>
		   <div class="text-center">
		   	<?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>
		      <a href="<?php echo $my_home_url; ?><?php _e( 'products', 'myesthetic' ); ?>/" class="btn btn-full btn-green btn-arrow section-btn"><?php _e( 'All products', 'myesthetic' ); ?></a>
		   </div>
		</section>

   <?php 
    else:
    	// Do not display nothing
    endif; ?>
  	<?php wp_reset_postdata();?>

<?php do_action( 'woocommerce_after_single_product' ); ?>
