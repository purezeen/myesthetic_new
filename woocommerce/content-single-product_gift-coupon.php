<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>


<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<!-- Product Header -->
	
	<section class="container coupon-header">
	  <div class="header-image">

	  <?php $hero_image = get_field( 'hero_image' ); ?>
	   <?php if ( $hero_image ) { ?>
	      <div class="header-image-wrap">
	          <img 
				src="<?php echo $hero_image['sizes']['thumbnail']; ?>" 
				
				srcset="<?php echo $hero_image['sizes']['thumbnail']; ?> 490w, <?php echo $hero_image['sizes']['image_tablet']; ?> 800w"

				width="<?php echo $hero_image['sizes']['thumbnail-width']; ?>"
	            height="<?php echo $hero_image['sizes']['thumbnail-height']; ?>"
	            
				alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" rel="preload" data-skip-lazy="" />
	     </div>
	   <?php }else if(has_post_thumbnail()){
	     $thumb_id = get_post_thumbnail_id();
	      ?>
	      <div class="header-image-wrap">

	    		<img src="<?php 
	        	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', false);
	        	echo $thumb_url = $thumb_url_array[0]; ?>" 
				
				srcset="<?php $thumb_url_array_small = wp_get_attachment_image_src($thumb_id, 'thumbnail', false);
				echo $thumb_url_array_small[0]; ?> 490w, <?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
				echo $thumb_url_array_medium[0]; ?> 800w",  sizes="(max-width: 1265px) 1265px, 100vw" 
				alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
			/>
	    </div>
	   <?php
	   } ?>

	  </div>
	  <div class="header-content">
	      <?php the_field( 'hero_content' ); ?>
	  </div>
	</section>
	<section class="coupon-layout container">
	   <div class="coupon-main">
	      <h1><?php _e( 'Gift coupon', 'myesthetic' ); ?></h1>

	      <div class="contact-form form-wrap">
	        <?php the_content(); ?>
				
			<?php
				add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
				function woocommerce_custom_single_add_to_cart_text() {
				    return __( 'Gutschein kaufen', 'myesthetic' );
				}
			 ?>
			 <div class="coupon-form">
			 	<?php do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' ); ?>
			 </div>
	      </div>
	   </div>
	  
	</section>

	<section class="container editor section coupon-flexible-section">
		<?php get_template_part('template-parts/flexible', 'content');?>
	</section>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
