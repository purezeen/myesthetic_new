<?php get_header(); ?>

<!-- Page header  -->
<section class="container header blog-header team-single">

   <div class="header-image">

      <?php $vertical_image = get_field( 'vertical_image' ); ?>
      <img class="desktop" src="<?php echo $vertical_image['sizes']['image_tablet']; ?>" alt="<?php echo $vertical_image['alt']; ?>" />

      <?php if (has_post_thumbnail( $post->ID ) ): ?>
         <?php $image_id = get_post_thumbnail_id(get_the_ID()); ?>
         <?php $alt_text = get_post_meta($image_id , '_wp_attachment_image_alt', true);?> 
         <div class="mobile">
            <img src="<?php echo the_post_thumbnail_url('medium') ?>" alt="<?php echo $alt_text ;?>">
         </div>
      <?php endif; ?>

   </div>

   <div class="header-content">
      <h1><?php the_title(); ?></h1>
      <span class="team-single__tagline"><?php the_field( 'job_title' ); ?>
      <?php if ( get_field( 'associate' ) == 1 ) { ?>
         <em><?php _e( 'Associate', 'myesthetic' ); ?></em>
      <?php } ?>
      </span>
      <div class="product-rating">
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <path id="svgStar" fill="currentColor"
               d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z">
            </path>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
         <svg xmlns="http://www.w3.org/2000/svg" class="star active" width="24" height="22" viewBox="0 0 576 512">
            <use xlink:href="#svgStar"></use>
         </svg>
      </div>
      <?php the_content(); ?>
   </div>
</section>

<?php if ( have_rows( 'select_the_treatment' ) ) : ?>
   <section class="container section pl-sm-0 pr-sm-0 treatments">
      <div class="section-title">
         <h2><?php _e( 'Treatments', 'myesthetic' ); ?></h2>
         <h3 class="section-subtitle"><?php echo _e('By', 'myesthetic'); ?> <?php the_title(); ?></h3>
      </div>
      <div class="column-3">
      <?php while ( have_rows( 'select_the_treatment' ) ) : the_row(); ?>
         <?php $post_object = get_sub_field( 'treatment' ); ?>
         <?php if ( $post_object ): ?>
            <?php $post = $post_object; ?>
            <?php setup_postdata( $post ); ?> 
               <div class="card">
                  <?php get_template_part('template-parts/loop', 'treatment'); ?>
               </div>
            <?php wp_reset_postdata(); ?>
         <?php endif; ?>
      <?php endwhile; ?>
      </div>
   </section>
<?php endif; ?>

<!-- Blog posts  -->
<?php $args = array(
   'post_type' => 'post',
   'posts_per_page' => 6,
   'post_status' => 'publish'
 );
?>

<?php $the_query = new WP_Query( $args ); ?>

<?php if ($the_query->have_posts()) :  ?>
   <section class="benefit benefit-orange benefit-blog clearfix">
      <div class="benefit-title">
         <h2><?php echo _e('Blog posts', 'myesthetic'); ?></h2>
      </div>
      <div class="slick-benefit-wrap">
         <div class="slick-benefit">
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <?php $post_author =  get_the_author_meta('first_name', $the_query ->post_author); ?>
               <div>
                  <div class="benefit-box">
                     <a href="<?php the_permalink(); ?>" class="blog-card">

                     <?php $blog_header_image = get_field( 'blog_header_image' ); ?>
                        <?php if ( $blog_header_image ) { ?>

                           <div class="blog-img cover" style="background-image: url('<?php echo $blog_header_image['sizes']['image_tablet']; ?>')">

                           </div>
                           
                           <?php } else if(has_post_thumbnail()) { ?>

                           <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'image_tablet' ); ?>

                           <div class="blog-img cover" style="background-image: url('<?php echo $backgroundImg[0]; ?>')"></div>
                        
                        <?php } ?>

                        <div class="blog-content">
                           <span class="blog-date"><?php the_time('d F Y'); ?><span class="separator">|</span>
                           <?php if(!empty($post_author)){
                               echo _e('by', 'myesthetic'); echo " "; echo $post_author;
                           }?></span></span>

                           <h2><?php the_title(); ?></h2>
                           <?php the_excerpt(); ?>
                           <span class="btn btn-link btn-link-arrow"><?php echo _e('Read more', 'myesthetic'); ?></span>
                        </div>
                     </a>
                  </div>
               </div>
            <?php endwhile; ?>
         </div>
      </div>
   </section>
<?php endif; ?>

<?php get_footer(); ?>


