<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */

get_header(); ?>

	<section class="container page-404">
			<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'gulp-wordpress' ); ?></h1>

	</section>

	
<?php
get_footer();
