<?php /* Template name: Single treatment */ get_header();

// Print the Service Scheme
if (shortcode_exists('product_schema')) {
	do_shortcode('[product_schema id='.get_the_ID().' type=service]');
} ?>

<?php $categories = get_the_category(); ?>
<?php //$cat_slug= array(); ?>
<?php foreach($categories as $category) {
   $cat_id[] = $category->term_id;
}?>

<?php $current_id =  apply_filters( 'wpml_object_id', 104, 'category', TRUE  ); ?>

<?php if (has_category( $current_id ,$post->ID)) {
   $extra_treatment = "true";
}else {
   $extra_treatment = "false";
} ?>

<?php 
$post_id = icl_object_id($post->ID, 'page', false, 'en'); 
?>

<section class="container single-treatment-header <?php echo ($extra_treatment == "true")? "single-treatment-header--orange" : "single-treatment-header--green"; ?>">
   <div class="header-image">
      <?php $hero_treatment_image = get_field( 'hero_treatment_image' ); ?>
      <?php if (  $hero_treatment_image ) { ?>
      <img src="<?php echo $hero_treatment_image['sizes']['large']; ?>"
         srcset="<?php echo $hero_treatment_image['sizes']['medium']; ?> 490w "
         sizes="(max-width: 1265px) 1265px, 100vw"
         alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_treatment_image['alt']; ?>"
         width="<?php echo $hero_treatment_image['sizes']['large-width']; ?>"
         height="<?php echo $hero_treatment_image['sizes']['large-height']; ?>" />
      <?php } else if(has_post_thumbnail()){
         $thumb_id = get_post_thumbnail_id(); ?>
         <img src="<?php $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', false);
            echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
            echo $thumb_url_array_medium[0]; ?> 490w " sizes="(max-width: 1265px) 1265px, 100vw"
         alt="<?php _e('my-esthetic', 'myesthetic'); ?>" width="<?php echo $thumb_url_array[1]; ?>"
         height="<?php echo $thumb_url_array[2]; ?>" />
      <?php } ?>
   </div>
   <div class="header-content">
   
      <?php $category = get_the_category();
      $categoru_id = $category[0]->term_id; ?>
      <?php
      // Define taxonomy prefix
      // Replace NULL with the name of the taxonomy eg 'category'
      $taxonomy_prefix = 'category';

      // Define term ID
      // Replace NULL with ID of term to be queried eg '123' 
      $term_id = $categoru_id;

      // Define prefixed term ID
      $term_id_prefixed = $taxonomy_prefix .'_'. $term_id; ?>

      <?php $category_image = get_field( 'category_image', $term_id_prefixed ); ?>
      <?php if ( $category_image ) { ?>
      <div class="card-icon">
         <img src="<?php echo $category_image['url']; ?>"
            alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $category_image['alt']; ?>" width="50"
            height="50" />
      </div>
      <?php } ?>

      <h1><?php the_title(); ?></h1>

      <?php if ($extra_treatment == "true"): ?>
      <span class="single-treatment__tagline"><?php _e('Extra treatments', 'myesthetic'); ?></span>
      <?php endif; ?>

      <?php the_field('hero_treatment_content'); ?>

      <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating]')): ?>
      <?php echo $wpdrating; ?>
      <?php else:
         if (shortcode_exists('product_rating')) {
            do_shortcode('[product_rating]');
         }
      endif; ?>

      <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>

      <?php global $post;
      $post_slug = $post->post_name; ?>
      <a href="<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>/"
         onclick="location='<?php echo $my_home_url; ?>/<?php _e( 'contact', 'myesthetic' ); ?>?treat=<?php echo $post_slug; ?>#treatments'; return false;"
         class="btn btn-full btn-arrow btn-green"><?php the_field( 'hero_button_text' ); ?></a>
      <!-- <div class="treatments-booked">*<span class="clients-number" data-id="<?php //echo $post_id; ?>"></span><?php //_e( ' costumers alredy had this treatments this month', 'myesthetic' ); ?></div> -->

   </div>
</section>

<section class="container layout-sidebar">
   <main class="single-treatment-container">
      <div class="single-treatment-content editor">
         <?php the_content(); ?>
         <?php get_template_part('template-parts/flexible', 'content');?>
      </div>
   </main>

   <aside class="sidebar">
      <div class="sidebar-box">
         <div class="sidebar-content">
            <h2><?php _e( 'Treatment info', 'myesthetic' ); ?></h2>
            <?php if ( have_rows( 'select_the_doctor' ) ) : ?>
               <?php while ( have_rows( 'select_the_doctor' ) ) : the_row(); ?>
                  <?php $post_object = get_sub_field( 'select_the_doctor' ); ?>
                  <?php if ( $post_object ): ?>
                     <?php $post = $post_object; ?>
                     <?php setup_postdata( $post ); ?> 
                        <a href="<?php the_permalink(); ?>" class="sidebar-dr-info">
                           <div class="associate__profile">
                              <div class="associate__profile--img">
                              <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                 <?php $image_id = get_post_thumbnail_id(get_the_ID()); ?>
                                 <?php $alt_text = get_post_meta($image_id , '_wp_attachment_image_alt', true);?> 
                                    <img src="<?php echo the_post_thumbnail_url('thumbnail') ?>" alt="<?php echo $alt_text ;?>">
                              <?php endif; ?>
                              </div>
                              <div class="associate__profile--content">
                                 <h3><?php the_title(); ?></h3>
                                 <p><?php the_field( 'job_title' ); ?></p>
                              </div>
                           </div>
                           <div class="btn btn-arrow"><?php _e( 'Learn more', 'myesthetic' ); ?></div>
                        </a>
                     <?php wp_reset_postdata(); ?>
                  <?php endif; ?>
               <?php endwhile; ?>
           
            <?php endif; ?>

            <?php if(get_field( 'duration' )): ?>
               <p><?php _e( 'Duration of intervention:', 'myesthetic' ); ?> <strong><?php the_field( 'duration' ); ?> <?php _e( 'min', 'myesthetic' ); ?></strong></p>
            <?php endif; ?>

            <?php the_field( 'treatment_single' ); ?>
            
            <?php if(get_field( 'treatment_price' )): ?>
               <h5><?php _e( 'Costs: ', 'myesthetic' ); ?>
               <strong>
               <?php if ( get_field( 'treatment_add_label_from' ) == 1 ) { 
                  echo _e( 'from', 'myesthetic' );
               } ?>
                <?php $treatment_price = get_field( 'treatment_price' ); ?>
               <?php echo $treatment_price; ?>€</strong></h5>
            <?php endif; ?>
            
         </div>
         <div class="sidebar-footer">
            <span class="sidebar-open"><?php _e( 'Treatment info', 'myesthetic' ); ?><span class="arrow-down"></span></span>
            <a href="<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>/"
               onclick="location='<?php echo $my_home_url; ?>/<?php _e( 'contact', 'myesthetic' ); ?>?treat=<?php echo $post_slug; ?>#treatments'; return false;" class="btn btn-full btn-green"><?php _e( 'Book appointment', 'myesthetic' ); ?></a>
         </div>
      </div>
   </aside>
</section>

<!-- Add flexible content section  -->
<?php get_template_part('template-parts/flexible', 'contentsection');?>

<?php $slider_images_images = get_field( 'slider_images' ); ?>
<?php if ( $slider_images_images ) :  ?>
   <section class="product-slider container">
      <div class="slick-product">

         <?php foreach ( $slider_images_images as $slider_images_image ): ?>

         <div>
            <div class="slider-product-wrap">
               <img src="<?php echo $slider_images_image['sizes']['product_slider']; ?>" width="350" height="350"
                  alt="<?php _e('my-esthetic', 'myesthetic'); ?>">
            </div>
         </div>
         <?php endforeach; ?>
      </div>
   </section>
<?php endif; ?>

<?php if($treatment_price >= 1000 || get_field( 'display_cta_for_installment_service' ) == 1): ?>

   <div class="container cta cta-contact section">
      <?php get_template_part('template-parts/cta', 'extratreatment');?>
   </div>

<?php else: ?>

   <div class="container cta cta-contact section">
      <?php get_template_part('template-parts/cta', 'contact');?>
   </div>

<?php endif; ?>

   <?php
   $categories = get_the_category();
   $category_name = $categories[0]->name;
   $do_not_duplicate = $post->ID;
    // WP_Query arguments
    $args = array(
      'category__in' => wp_get_post_categories( get_queried_object_id() ),
      'post_type' => 'treatment',
      'post__not_in' => array($do_not_duplicate),
      'orderby' => 'rand',
      'posts_per_page' => 3,
    );

    // The Query
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) : ?>
      <section class="similar-treatment container section">
         <div class="text-center">
            <h2 class="section-title"><?php _e( 'Similar treatments', 'myesthetic' ); ?></h2>
         </div>
         <div class="column-3 treatments">

            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <div class="card card-rating similar-treatment-card">
               <?php get_template_part('template-parts/loop', 'treatment')?>
            </div>
            <?php endwhile; ?>

         </div>
         <div class="text-center">
            <a href="<?php echo $my_home_url; ?><?php _e( 'treatments', 'myesthetic' ); ?>/" class="btn btn-full btn-green btn-arrow section-btn"><?php _e( 'All treatments', 'myesthetic' ); ?></a>
         </div>
      </section>
   <?php endif; 
    wp_reset_postdata(); ?>

<?php get_footer(); ?>