(function($) {

    $(document).ready(function() {
        /*----------  Google reviews  ----------*/
        var reviews = $(document).find("#google-reviews");
        if (reviews.length > 0) {
            reviews.googlePlaces({
                placeId: 'ChIJAWMThj7KuEcRDGvqhmC-CcM',
                render: ['reviews', 'name'],
                min_rating: 5,
                max_rows: 0
            });

            $("#map-plug").remove();
        }

        /*----------  Testimonials slider  ----------*/
        if ($(".slick-testimonials")[0]) {
            setTimeout(function() {
                $('.slick-testimonials').slick({
                    infinite: true,
                    centerPadding: '0px',
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    centerMode: true,
                    draggable: false,
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            draggable: false
                        }
                    }]
                });
            }, 2000);
        }

        // Add read more, read less button on testimonial content 
        var showChar = 470;
        var ellipsestext = "";
        var moretext = "Read More";
        var lesstext = "Read Less";
        $('.content-wrap').each(function() {

            var content = $(this).html();
            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="#" class="morelink testimonial-more"><span class="arrow-down"></span>' + moretext + '</a></span>';
                $(this).html(html);
            }
        });

        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html('<span class="arrow-down"></span>' + moretext);
            } else {
                $(this).addClass("less");
                $(this).html('<span class="arrow-down"></span>' + lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

        /*----------  Benefit slider  ----------*/

        if ($('.slick-benefit')[0]) {
            $('.slick-benefit').slick({
                dots: false,
                infinite: true,
                speed: 800,
                slidesToScroll: 1,
                variableWidth: true,

                responsive: [{
                        breakpoint: 990,
                        settings: {
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: false,
                        }
                    }
                ]
            });
            $('.slick-benefit').on('setPosition', function() {
                jbResizeSlider();
            });
        }

        // we need to maintain a set height when a resize event occurs.
        // Some events will through a resize trigger: 
        $(window).trigger('resize');
        $(window).on('resize', function(e) {
            jbResizeSlider();
        });

        // since multiple events can trigger a slider adjustment, we will control that adjustment here
        function jbResizeSlider() {
            $slickSlider = $('.slick-benefit');
            $slickSlider.find('.slick-slide').height('auto');

            var slickTrack = $slickSlider.find('.slick-track');
            var slickTrackHeight = $(slickTrack).height();
            $slickSlider.find('.slick-slide').css('height', slickTrackHeight + 'px');
        }

        /*----------  Product slider  ----------*/


        if ($(".slick-product")[0]) {
            $('.slick-product').slick({
                centerPadding: '0',
                slidesToShow: 3,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerPadding: '40px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }

        /*----------  Home page effects slider  ----------*/

        if ($(".slick-effects")[0]) {
            var $sliderbenefit = $('.slick-effects');

            $sliderbenefit.slick({
                speed: 1000,
            });

            if ($sliderbenefit.length) {
                var currentSlide;
                var slidesCount;
                var sliderCounter = document.createElement('div');
                sliderCounter.classList.add('slider-counter');

                var updateSliderCounter = function(slick, currentIndex) {
                    currentSlide = slick.slickCurrentSlide() + 1;

                    slidesCount = $(".slick-effects").slick("getSlick").slideCount;

                    $(sliderCounter).html(currentSlide.toString().padStart(2, '0') + '<span class="separator"> / </span> ' + "<span class='total-count'> " + slidesCount.toString().padStart(2, '0') + "</span>")
                };

                $sliderbenefit.on('init', function(event, slick) {

                    $sliderbenefit.append(sliderCounter);
                    updateSliderCounter(slick);
                });

                $sliderbenefit.on('afterChange', function(event, slick, currentSlide) {
                    updateSliderCounter(slick, currentSlide);
                });
                $sliderbenefit.slick("unslick");
                $sliderbenefit.slick();
            }

        }

    });

})(jQuery);