(function($) {
    $(document).ready(function() {

        // Header phone button 
        $('.phone').on('mouseover', function() {
            $('.show-phone-details').fadeIn(300);
        });

        $('.phone, .phone-wrap').on('mouseleave', function() {
            $('.show-phone-details').fadeOut(300)
        });

        if ($('body').hasClass('page-template-contact-template')) {
            function onshowelement() {
                $('.gform_body select').chosen('destroy');
                $(".gform_body select").chosen();
            }

            function load_hash_form() {
                var tabDiv = window.location.hash;
                var tabDivId = tabDiv.split('#')[1];

                if (tabDivId == undefined) {
                    $('.tabwrap').hide();
                    $('#freeconsultation').fadeIn();
                    $('.freeconsultation').addClass('active');
                    onshowelement();
                } else {
                    $('.tabwrap').hide();
                    $('#' + tabDivId).fadeIn();
                    $('#tabs a').removeClass('active');
                    $('.' + tabDivId).addClass('active');
                    onshowelement();
                }
            }

            // Load on ready
            load_hash_form();

            // ADD Lisener on hashchange
            $(window).on('hashchange', function() {
                load_hash_form();
                onshowelement();
            });
        }

        /* Navigation */
        $('.burger').click(function() {
            $('.site-header').toggleClass('open');
        });


        // change is-checked class on buttons
        $('.button-group').each(function(i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'span', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });

        if ($(window).width() < 768) {
            $('.toggle').on('click', '.preventclick', function(e) {
                e.preventDefault();
                $(this).closest('.toggle').addClass('active');
            });
        }

        // Toggle text 
        $('.toggle').on('click', 'button', function(e) {
            $(this).closest('.toggle').toggleClass('active');
        });

        $('.faq-toggle').on('click', 'button', function(e) {
            $(this).closest('.faq-toggle').find('.toggle-content').slideToggle();
            $(this).closest('.faq-toggle').toggleClass('active');
        });

        $('.faq-toggle').on('click', 'h3', function(e) {
            $(this).closest('.faq-toggle').find('.toggle-content').slideToggle();
            $(this).closest('.faq-toggle').toggleClass('active');
        });

        // ===== Scroll to Top ==== 
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 200) { // If page is scrolled more than 50px
                $('.scroll-to-top').fadeIn(200); // Fade in the arrow
            } else {
                $('.scroll-to-top').fadeOut(200); // Else fade out the arrow
            }
        });

        $('.scroll-to-top').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        // trigger number counter on scroll
        if ($('body').hasClass('home')) {
            var waypoint = new Waypoint({
                element: document.getElementById('number-counter'),
                handler: function() {

                    $('.count-wrap').each(function() {
                        var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 2000,
                            step: function(func) {
                                $(this).text(parseFloat(func).toFixed(size));
                            }
                        });
                    });

                    $('.border-box').each(function() {

                    });

                    this.destroy()
                },
                offset: '70% '
            });

            $(document).ready(function() {
                var waypoint = new Waypoint({
                    element: document.getElementById('number-counter'),
                    handler: function() {
                        window.addEventListener('scroll', function() {
                            var scrollPosition = window.pageYOffset;
                            var bgParallax = document.getElementsByClassName('parallax_bg')[0];
                            var bgParallax_1 = document.getElementsByClassName('parallax_bg')[1];
                            var paralax_container = document.getElementById('number-counter');
                            var limit = paralax_container.offsetTop + paralax_container.offsetHeight;
                            if (bgParallax_1 && bgParallax_1.hasOwnProperty('style')) {
                                bgParallax_1.style.backgroundPositionY = (100 - 100 * scrollPosition / limit) + '%';
                            }
                        });
                    },
                    offset: '70% '
                });
            });
        };

        $('.more').on('click change', '#toggle', function() {
            var elem = $(this).text();
            $(this).closest('.more').find('.more-text').slideToggle();
            $(this).closest('.more').toggleClass('active');
        });

        // Lightcase plugin
        $('a[data-rel^=lightcase]').lightcase({
            maxWidth: 1000
        });

        // On click open sidebar menu 
        $('.sidebar-open').on('click', function() {
            $('.sidebar').toggleClass('active');
        });

        // Custom file upload 
        $(".ginput_container_fileupload").on("click", 'input', function() {
            $(".file-upload-name").remove();
        });

        $(".ginput_container_fileupload").on("change", "input", function() {
            $(this).parents('.ginput_container_fileupload').append("<span class='file-upload-name'></span>");
            $(".file-upload-name").text($(this).val().replace(/.*(\/|\\)/));
        });

        // Open modal on click 
        $('.wpdiscuz-rating').on('click', function() {
            $('.wc_comment').val('Rate' + 99876934 * Math.random());
        })

        $('.modal-box .wc_comm_form').submit(function(e) {
            e.preventDefault();
            $('.success-form').fadeIn();
            setTimeout(function() {
                $('.modal').fadeOut();
            }, 2500)
        });

        $('.open-modal').on('click', function() {
            $('.modal').fadeIn();
            $('.success-form').hide();
            $('.modal').css('display', 'flex');
        });

        $('.modal-close-btn').on('click', function() {
            $('.modal').fadeOut();
        });

        $(document).click(function(event) {
            //if you click on anything except the modal itself or the "open modal" link, close the modal

            if (!$(event.target).closest(".modal-box,.open-modal").length) {
                $("body").find(".modal").fadeOut();
            }
        });

        $(document).on('click', '.single_add_to_cart_button, .woo-single-product', function(e) {
            // Ignore gift and coupon card products
            if ($('body').hasClass('postid-5697') || $('body').hasClass('postid-5590')) {
                return;
            }

            // Prevent button click, and add the product to the cart using AJAX request
            e.preventDefault();
            var $thisbutton = $(this);

            // If is product page get product ID from url
            if ($thisbutton.hasClass('woo-single-product')) {
                var href = $thisbutton.attr('href')
                var href_id = parseInt(href.match(/add-to-cart=(\d+)/)[1])
            }

            // Woo single page get values
            let $form = $thisbutton.closest('form.cart');
            let id = $thisbutton.val() || href_id;
            let product_qty = $form.find('input[name=quantity]').val() || 1;
            let product_id = $form.find('input[name=product_id]').val() || id;
            let variation_id = $form.find('input[name=variation_id]').val() || 0;
            var data = {
                action: 'ql_woocommerce_ajax_add_to_cart',
                product_id: product_id,
                product_sku: '',
                quantity: product_qty,
                variation_id: variation_id,
            };

            $.ajax({

                type: 'post',

                url: wc_add_to_cart_params.ajax_url,

                data: data,

                beforeSend: function(response) {

                    $thisbutton.removeClass('added').addClass('loading');
                    // str = JSON.stringify(response);
                    // str = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                    // console.log("beforeSend " + str);
                },

                // error: function (response) {
                //     str = JSON.stringify(response);
                //     str = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                //     console.log("error " + str);
                // },

                complete: function(response) {
                    setTimeout(function() {
                        $thisbutton.addClass('added').removeClass('added').removeClass('loading');
                    }, 2000);
                },

                success: function(response) {

                    if (response.error & response.product_url) {

                        window.location = response.product_url;

                        return;

                    } else {

                        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);

                    }

                },

            }).done(function() {
                $count_num = $(".cart-items-count").text();
                $minicart_items = $(".woocommerce-mini-cart.cart_list > li");
                $gift = false;
                $coupon = false;
                $minicart_items.each(function(index, el) {
                    if ($(this).hasClass('gift-item')) $gift = true;
                    if ($(this).hasClass('coupon-item')) $coupon = true;
                });
                // console.log('ADD');
                // console.log('Gift = ' + $gift + ' / Coupon = ' + $coupon);

                if (!$gift && $count_num >= "2") {
                    if ($coupon && $count_num === "2") {
                        // do nothing
                    } else {
                        gift_product_add();
                    }
                }

                // if ($count_num === "2" || $count_num === "3") {
                //   gift_product_add();
                // }
            });


        });
        $('body').on('removed_from_cart', function() {
            $count_num = $(".cart-items-count").text();
            $minicart_items = $(".woocommerce-mini-cart.cart_list > li");
            $gift = false;
            $coupon = false;
            $minicart_items.each(function(index, el) {
                if ($(this).hasClass('gift-item')) $gift = true;
                if ($(this).hasClass('coupon-item')) $coupon = true;
            });
            // console.log('REMOVE');
            // console.log('Gift = ' + $gift + ' / Coupon = ' + $coupon);
            // console.log($count_num);
            if ($count_num >= "2") {
                if ($gift && $count_num <= "2") {
                    gift_product_remove();
                }
                if ($coupon && $gift && $count_num == "3") {
                    gift_product_remove();
                }
            }
        });

        function gift_product_add() {
            var product_id = document.documentElement.lang === "de-DE" ? 5445 : 5446,
                product_qty = 1,
                variation_id = 0;

            var data = {
                action: 'ql_woocommerce_ajax_add_to_cart',
                product_id: product_id,
                quantity: product_qty,
            };

            $.ajax({
                type: 'post',
                url: wc_add_to_cart_params.ajax_url,
                data: data,
                success: function(response) {
                    if (response.error & response.product_url) {
                        window.location = response.product_url;

                        return;
                    } else {
                        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
                    }
                }
            }).done(function(response) {
                // console.log(response);
            });
        }

        function gift_product_remove() {
            var product_id = document.documentElement.lang === "de-DE" ? 5445 : 5446;
            $('a[data-product_id*="' + product_id + '"]').click();
        }

        // Hide Header on on scroll down
        'use strict';
        var c, currentScrollTop = 0,
            navbar = $('.site-header');

        $(window).scroll(function() {
            var a = $(window).scrollTop();
            var b = navbar.height();

            currentScrollTop = a;
            if (c < currentScrollTop && a > b + b) {
                navbar.addClass("scrollUp");

                if ($(window).width() > 990) {
                    $('.mini-cart-wrap').removeClass('mini-cart-active');
                }

            } else if (c > currentScrollTop && !(a <= b)) {
                navbar.removeClass("scrollUp");
            }
            c = currentScrollTop;

            if (a > 380) {
                navbar.addClass("nav-white");
            } else {
                navbar.removeClass("nav-white");
            }
        });

        // Mini cart animation 
        $(document).click(function(event) {
            if (!$(event.target).closest(".dropdown-menu-mini-cart, .add-to-card, .dropdown-back").length) {
                $("body").find(".mini-cart-wrap").removeClass('mini-cart-active');
            }
        });

        if ($(window).width() > 990) {
            $('.dropdown-back').on('mouseover', function() {
                $('.mini-cart-wrap').addClass('mini-cart-active');
            })
        } else {
            $('.dropdown-back').on('click', function() {
                $('.mini-cart-wrap').addClass('mini-cart-active');
            })
        }

        $('.dropdown-back').on('click', function() {
            $('.mini-cart-wrap').addClass('mini-cart-active');
        })

        $(document.body).on('added_to_cart', function() {
            setTimeout(function() {
                $('.mini-cart-wrap').addClass('mini-cart-active');
            }, 500);

            if ($('.site-header').hasClass('scrollUp')) {
                $('.site-header').removeClass('scrollUp');
            }
        });

        $("body").on('DOMSubtreeModified', ".cart-items-count", function() {
            var cart_count = $('.cart-items-count').text();
            if (cart_count == 0) {
                setTimeout(function() {
                    $('.mini-cart-wrap').addClass('mini-cart-active');
                }, 500);
            }
        });

        // Filters - treatments and product 
        var $filters = $('#filters [data-filter]'),
            $grid_item = $('.grid [data-item]');

        $filters.on('click', function(e) {
            e.preventDefault();
            var $this = $(this);

            $filters.removeClass('active');
            $this.addClass('active');

            var $filterItem = $this.attr('data-filter');
            if ($filterItem == 'all') {
                $('.grid-item.cta').fadeIn();
                $('.grid-item.banner-section').fadeIn();
                $grid_item.removeClass('is-animated')
                    .fadeOut().promise().done(function() {
                        $grid_item.addClass('is-animated').fadeIn();
                    });
            } else {
                $('.grid-item.cta').fadeOut();
                $('.grid-item.banner-section').fadeOut();
                $grid_item.removeClass('is-animated')
                    .fadeOut().promise().done(function() {
                        $grid_item.filter('[data-item = "' + $filterItem + '"]')
                            .addClass('is-animated').fadeIn();
                    });
            }
        });

        if ($('body').hasClass('page-template-products-template')) {
            var url = $(location).attr('hash');
            if (url) {
                hash = url.replace('#', '');
                $filters.removeClass('active is-checked');
                $('#filters [data-filter="' + hash + '"]').addClass('is-checked active');
                $('.grid-item.cta').fadeOut();
                $('.grid-item.banner-section').fadeOut();
                $grid_item.removeClass('is-animated')
                    .fadeOut().promise().done(function() {
                        $grid_item.filter('[data-item = "' + hash + '"]')
                            .addClass('is-animated').fadeIn();
                    });
            }
        }
    });

    // Moved inlimne styles 
    $(document).find('form.cart').on('click', 'button.plus, button.minus', function() {
        // Get current quantity values
        var qty = $(this).closest('form.cart').find('.qty');
        var val = parseFloat(qty.val());
        var max = parseFloat(qty.attr('max'));
        var min = parseFloat(qty.attr('min'));
        var step = parseFloat(qty.attr('step'));

        // Change the value if plus or minus
        if ($(this).is('.plus')) {
            if (max && (max <= val)) {
                qty.val(max);
            } else {
                qty.val(val + step);
            }
        } else {
            if (min && (min >= val)) {
                qty.val(min);
            } else if (val > 1) {
                qty.val(val - step);
            }
        }
    });

    // Update card
    $(document).ready(function() {
        function update_cart() {
            $('.quantity').on('click', '.plus', function() {
                // console.log('plus');
                var input = $(this).parent('.arrow-wrap').prev('input.qty');
                var val = parseInt(input.val());
                var step = input.attr('step');
                step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
                input.val(val + step).change();
                $("[name='update_cart']").trigger("click");
            });

            $('.quantity').on('click', '.minus', function() {
                // console.log('minus');
                var input = $(this).parent('.arrow-wrap').next('input.qty');
                var val = parseInt(input.val());
                // console.log("minus" + val)
                var step = input.attr('step');
                step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
                if (val > 0) {
                    input.val(val - step).change();
                }
                $("[name='update_cart']").trigger("click");
            });
        }

        update_cart();

        $(document.body).on('updated_cart_totals', function() {
            update_cart()
        });

        jQuery('.shortcode p br').remove();
    });

    // Single treatment page - reserved treatments
    $(document).ready(function() {
        if ($('.clients-number')[0]) {
            var treatment = $('.clients-number').attr("data-id");
            var treatment_last = treatment % 10;
            // var treatment_lasttwo = treatment % 100;
            // var treatmnet_beforelast = Math.round(treatment_lasttwo / 10) % 10;
            // var treatment_num = treatment_last + treatmnet_beforelast;

            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth();
            var clients_num = Math.round((day + treatment_last + month) / 1.4);

            if (day >= 10) {
                // clients_num += 5;
            }
            if (clients_num < 3) {
                clients_num += 3;
            }

            $('.clients-number').text(clients_num);
        }
    });

})(jQuery);