(function ($) {

  $(document).ready(function () {
    // Get stylesheet_directory_uri frob the body tag
    $('.contact-map-wrap').on('click', function() {
      $(this).removeClass('contact-map-wrap-img');
      var stylesheet_directory_uri = $(document).find('body').attr('data-sduri');
      mapboxgl.accessToken = 'pk.eyJ1IjoiaXZhbmFpa2EiLCJhIjoiY2p3MG1nY2lkMGNrMDRhcndzNDVyZXoyaSJ9.STwhzlwrUlcvPQUDkFr1Zg';
      var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/ivanaika/ck1vxjzqk0a2c1cp8oyncw21h',
        center: [6.779354, 51.220317],
        zoom: 15,
      });

      map.on('load', function () {
        map.loadImage(
          '' + stylesheet_directory_uri + '/img/pin.png',
          function (error, image) {
            if (error) throw error;
            map.addImage('cat', image);
            map.addSource('point', {
              'type': 'geojson',
              'data': {
                'type': 'FeatureCollection',
                'features': [{
                  'type': 'Feature',
                  'geometry': {
                    'type': 'Point',
                    'coordinates': [6.779354, 51.220317]
                  }
                }]
              }
            });
            map.addLayer({
              'id': 'points',
              'type': 'symbol',
              'source': 'point',
              'layout': {
                'icon-image': 'cat',
                'icon-size': 0.05
              }
            });
          }
        );
      });
    });
    
  });
})(jQuery);