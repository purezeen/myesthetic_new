<?php
/* Template name: Single product */
get_header(); 

// Print the Product Scheme
if (shortcode_exists('product_schema')) {
	do_shortcode('[product_schema id='.get_the_ID().' type=product]');
} ?>

<?php if ( get_field( 'show_promotion', 'option' ) == 1 ) { 
    $header_promo = "header-promo-single";
  } ?>

<section class="container single-product-header <?php echo $header_promo; ?>">


   <?php 
   $woo_product_categories = get_the_terms( get_the_ID(), 'product_cat' );
   foreach($woo_product_categories as $woo_product_cat) {
      if ( $woo_product_cat->parent !== 0 ) {
         $cat_product_name = $woo_product_cat->name;
      }
   } ?>

   <div class="single-product-title">
      <span class="single-product-cat"><?php echo $cat_product_name; ?></span>
      <h1><?php the_title(); ?></h1>
   </div>

   <div class="single-product-content">
      <div class="single-product-price">
         <div class="h6"><?php _e( 'Price:', 'myesthetic' ); ?> <strong><?php the_field( 'product_price' ); ?> <span class="currency"><?php echo $symbol = get_woocommerce_currency_symbol(); ?></span></strong></div>
         <?php if (shortcode_exists('wpdrating') && $wpdrating = apply_shortcodes('[wpdrating google="true"]')): ?>
            <?php echo $wpdrating; ?>
         <?php else:
            if (shortcode_exists('product_rating')) {
	            do_shortcode('[product_rating]');
            }
         endif; ?>
      </div>

      <?php $hero_image = get_field( 'hero_image' ); ?>
      <?php if ( $hero_image ) { ?>
         <div class="single-product-image">
            <img src="<?php echo $hero_image['sizes']['medium']; ?>" alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" width="490" height="490" />
         </div>
      <?php } else if(has_post_thumbnail()){
         $thumb_id = get_post_thumbnail_id(); ?>
         <div class="single-product-image">
            <img src="<?php 
               $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', false);
               echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
               echo $thumb_url_array_medium[0]; ?> 490w" sizes="(max-width: 1265px) 1265px, 100vw" 
               width="490" 
               height="490"
               alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
            />
         </div>
      <?php } ?>
   </div>

   <div class="single-product-btn add-to-card-animation">
      <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>
       <?php 
         global $post;
         $post_slug = $post->post_name; ?>

      <a href="<?php echo $my_home_url; ?>produkte?add-to-cart=<?php echo $post->ID; ?>" class="btn btn-green btn-full btn-arrow woo-single-product add-to-card">
         <span><?php _e( 'Order your product now', 'myesthetic' ); ?></span>
         <div class="loading-icon">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
            <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
            </svg></div>
            <div class="load-finish">
            <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
               <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
                  stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
            </svg>
            </div>
      </a>

   </div>

   <?php if ( get_field( 'show_promotion', 'option' ) == 1 ) { ?>
		<div class="header-promo-wrap">
			<div class="header-promo-single-banner">
				<?php the_field( 'promotion_text', 'option' ); ?>
			</div>
		</div>
		<?php } ?>
</section>

<section class="container layout-sidebar">
   <main class="single-treatment-container">
      <div class="single-treatment-content editor">

         <?php the_content(); ?>

         <!-- Flexible content  -->
         <?php if ( have_rows( 'flexible_content' ) ): ?>
         <?php while ( have_rows( 'flexible_content' ) ) : the_row(); ?>
         <?php if ( get_row_layout() == 'faq' ) : ?>

         <!-- Faq  -->
         <div class="faq">
            <div class="faq-toggle">
               <button class="arrow-down"></button>
               <h6><?php the_sub_field( 'faq_question' ); ?></h6>
               <div class="toggle-content">
                  <?php the_sub_field( 'faq_answer' ); ?>
               </div>
            </div>
         </div>

         <!-- Text editor  -->
         <?php elseif ( get_row_layout() == 'text_editor' ) : ?>
            <?php the_sub_field( 'text_editor' ); ?>
         <?php endif; ?>

         <?php endwhile; ?>
         <?php else: ?>
         <?php // no layouts found ?>
         <?php endif; ?>

      </div>
   </main>

   <!-- Sidebar product  -->
   <aside class="sidebar">
      <div class="sidebar-box">
         <div class="sidebar-content">
            <h2><strong><?php _e( 'Product info', 'myesthetic' ); ?></strong></h2>
            <?php the_field( 'treatment_single' ); ?>
            <?php if (get_field('product_price')) { ?>
               <div class="h5"><?php _e( 'Price:', 'myesthetic' ); ?> <strong><?php the_field( 'product_price' ); ?> EUR</strong></div>
            <?php } ?>
         </div>
         <div class="sidebar-footer add-to-card-animation">
            <span class="sidebar-open"><?php _e( 'Product info', 'myesthetic' ); ?><span
               class="arrow-down"></span></span>

            <a href="<?php echo $my_home_url; ?>produkte?add-to-cart=<?php echo $post->ID; ?>" class="btn btn-green btn-full woo-single-product add-to-card">
               <span class="label"><?php _e( 'Order now', 'myesthetic' ); ?></span>
               <div class="loading-icon">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                  <path fill="#fff" d="M12.9 3.1c1.3 1.2 2.1 3 2.1 4.9 0 3.9-3.1 7-7 7s-7-3.1-7-7c0-1.9 0.8-3.7 2.1-4.9l-0.8-0.8c-1.4 1.5-2.3 3.5-2.3 5.7 0 4.4 3.6 8 8 8s8-3.6 8-8c0-2.2-0.9-4.2-2.3-5.7l-0.8 0.8z"></path>
                  </svg></div>
                  <div class="load-finish">
                  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                     <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2"
                        stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />
                  </svg>
               </div>
            </a>
            <!-- <a href="<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>/" onclick="location='<?php echo $my_home_url; ?><?php _e( 'contact', 'myesthetic' ); ?>?prod=<?php echo $post_slug; ?>#products'; return false;" class="btn btn-full btn-green"><?php _e( 'Order now', 'myesthetic' ); ?></a> -->
         </div>
      </div>
   </aside>
</section>

<!-- Add flexible content section  -->
<?php get_template_part('template-parts/flexible', 'contentsection');?>

<?php $slider_images_images = get_field( 'slider_images' ); ?>
<?php if ( $slider_images_images ) :  ?>
<section class="product-slider container">
   <div class="slick-product">

      <?php foreach ( $slider_images_images as $slider_images_image ): ?>

      <div>
         <div class="slider-product-wrap">
            <img src="<?php echo $slider_images_image['sizes']['product_slider']; ?>" width="350" height="350" alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $slider_images_image['alt'];?>">
         </div>
      </div>
      <?php endforeach; ?>
   </div>
</section>

<?php endif; ?>

<div class="container cta cta-contact section">
   <?php get_template_part('template-parts/cta', 'contact');?>
</div>

<section class="similar-product container section">
   <div class="text-center">
      <h2 class="section-title"><?php _e( 'Similar products', 'myesthetic' ); ?></h2>
   </div>

   <?php
    $do_not_duplicate = $post->ID;
    if($category) {
      $category_name = $category[0]->name;
    }else {
      $category_name = "";
    }
   
    // WP_Query arguments
    $args = array(
      'category__in' => wp_get_post_categories( get_queried_object_id() ),
      'post_type' => 'product',
      'post__not_in' => array($do_not_duplicate, 5590, 5445, 5697, 5446),
      'posts_per_page' => 3,
      'orderby' => 'rand',
    );

    // The Query
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) : ?>

      <div class="product-list column-3">

         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
         <div class="card-product">
            <?php get_template_part('template-parts/loop', 'product')?>
         </div>
         <?php endwhile; ?>

      </div>

      <?php endif; wp_reset_postdata(); ?>

      <div class="text-center">
         <a href="<?php echo $my_home_url; ?><?php _e( 'products', 'myesthetic' ); ?>/" class="btn btn-full btn-green btn-arrow section-btn"><?php _e( 'All products', 'myesthetic' ); ?></a>
      </div>
</section>

<?php get_footer(); ?>
