<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

/*------------------------------------*\
    Theme Support
\*------------------------------------*/
add_theme_support('woocommerce');

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on gulp-wordpress, use a find and replace
     * to change 'gulp-wordpress' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'image_tablet', 800 );
    add_image_size( 'product_slider', 350, 350, true );
    
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'gulp-wordpress' ),
        'footer_links'  => __( 'Footer links', 'gulp-wordpress' ),
        'footer_treatments'  => __( 'Footer treatments', 'gulp-wordpress' ),
        'header-cart'  => __( 'Header cart', 'gulp-wordpress' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     * See https://developer.wordpress.org/themes/functionality/post-formats/
     */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {
    wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri() , array(), '11', 'all');

    wp_enqueue_script('noframework.waypoints.min', get_template_directory_uri('jquery') . '/js/noframework.waypoints.min.js', array(), '2', true);

    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '2.987658909856787447798', true);

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    wp_enqueue_style('custom-style', get_template_directory_uri(  ) . '/custom-style.css');
}


function mytheme_enqueue_front_page_scripts() {
    if( is_front_page() )
    {
        wp_enqueue_script( 'google_reviews', get_template_directory_uri() . '/js/google-reviews.js', array( 'jquery' ), null, true );

    }

    if (is_single() || is_front_page() || is_author())  {
        wp_enqueue_style('slick-css', get_template_directory_uri(  ) . '/css/slick.css');

        wp_enqueue_style('slick-theme', get_template_directory_uri(  ) . '/css/slick-theme.css');
    
        wp_enqueue_script('slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '201', true);
        
        wp_enqueue_script('sliders', get_template_directory_uri() . '/js/sliders.js', array('jquery'), '2.56', true);

    }

    if ( is_page_template( 'template/contact-template.php' ) ) {

        wp_enqueue_script( 'map_box_js', 'https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js', array('jquery'), '2', true );

        wp_enqueue_script( 'map-box', get_template_directory_uri() . '/js/map_box.js', array('jquery'), '2', true );
    }
    // if( !is_front_page() )
    // {
        wp_enqueue_style( 'lightcase-css',  get_template_directory_uri() . '/css/lightcase.css' );
        wp_enqueue_script( 'lightcase', get_template_directory_uri() . '/js/lightcase.js', array('jquery'), '2', true );
    // }
}
add_action( 'wp_enqueue_scripts', 'mytheme_enqueue_front_page_scripts' );
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Create Treatment CPT
function create_post_type_treatment()
{
    register_taxonomy_for_object_type('category', 'myesthetic'); // Register Taxonomies for Category
    register_post_type('treatment', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Treatments', 'myesthetic'), // Rename these to suit
            'singular_name' => __('Treatment', 'myesthetic'),
            'add_new' => __('Add New', 'myesthetic'),
            'add_new_item' => __('Add New Treatment', 'myesthetic'),
            'edit' => __('Edit', 'myesthetic'),
            'edit_item' => __('Edit Treatment', 'myesthetic'),
            'new_item' => __('New Treatment', 'myesthetic'),
            'view' => __('View Treatment', 'myesthetic'),
            'view_item' => __('View Treatment', 'myesthetic'),
            'search_items' => __('Search Treatment', 'myesthetic'),
            'not_found' => __('No Treatments found', 'myesthetic'),
            'not_found_in_trash' => __('No Treatments found in Trash', 'myesthetic')
        ),
        'public' => true,
        'hierarchical' => false,
        'has_archive' => false,
        'menu_icon' => 'dashicons-feedback',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            'comments',
            'revisions',
            'author'
        ),
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'category'
        ), // Add Category support
    ));
}



// Create new category type of boards 
function create_associate_cat() {
 
    $labels = array(
      'name' => _x( 'Associate categories(relationship with user)', 'taxonomy general name' ),
      'singular_name' => _x( 'Associate categories', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Types' ),
      'all_items' => __( 'All Types' ),
      'parent_item' => __( 'Parent Type' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Edit Type' ), 
      'update_item' => __( 'Update Type' ),
      'add_new_item' => __( 'Add New Type' ),
      'new_item_name' => __( 'New Type Name' ),
      'menu_name' => __( 'Associate categories' ),
    ); 	
   
    register_taxonomy('associate',array('treatment'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'associate' ),
    ));
  }
  add_action( 'init', 'create_associate_cat', 0 );

add_action( 'init', 'create_post_type_treatment', 0 );


// Register Custom Post Type
function cpt_custom_block() {

	$labels = array(
		'name'                  => _x( 'Custom block', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Custom block', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Custom block', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Custom block', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Custom block', 'gulp_wordpress' ),
		'description'           => __( 'Custom block  Description', 'gulp_wordpress' ),
		'public' => true,
        'supports' => array(
            'title',
        ),
		'exclude_from_search' => true,
		'show_in_admin_bar'   => false,
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => false,
		'query_var'           => false
	);
	register_post_type( 'custom_block', $args );

}

add_action( 'init', 'cpt_custom_block', 0 );


// Create Treatment CPT

function create_post_type_team()

{

    register_taxonomy_for_object_type('category', 'myesthetic'); // Register Taxonomies for Category

    register_post_type('Team', // Register Custom Post Type

        array(

        'labels' => array(

            'name' => __('Team', 'myesthetic'), // Rename these to suit

            'singular_name' => __('Team', 'myesthetic'),

            'add_new' => __('Add New', 'myesthetic'),

            'add_new_item' => __('Add New team', 'myesthetic'),

            'edit' => __('Edit', 'myesthetic'),

            'edit_item' => __('Edit team', 'myesthetic'),

            'new_item' => __('New team', 'myesthetic'),

            'view' => __('View team', 'myesthetic'),

            'view_item' => __('View team', 'myesthetic'),

            'search_items' => __('Search team', 'myesthetic'),

            'not_found' => __('No team found', 'myesthetic'),

            'not_found_in_trash' => __('No team found in Trash', 'myesthetic')

        ),

        'public' => true,

        'hierarchical' => false,

        'has_archive' => false,

        'menu_icon' => 'dashicons-id',

        'supports' => array(

            'title',

            'editor',

            'thumbnail',

            'excerpt',

            'comments',

            'revisions'

        ),

        'can_export' => true, // Allows export in Tools > Export

        'taxonomies' => array(

            'category'

        ), // Add Category support

    ));

}


add_action( 'init', 'create_post_type_team', 0 );



//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Options',
        'menu_title'    => 'Theme settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
    return 20;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



add_filter( 'gform_confirmation_anchor', '__return_true' );



// Dynamically Populating Multi-Select Fields with treatment posts 
add_filter( 'gform_pre_render_18', 'treatment_posts' );
add_filter( 'gform_pre_validation_18', 'treatment_posts' );
add_filter( 'gform_pre_submission_filter_18', 'treatment_posts' );
add_filter( 'gform_admin_pre_render_18', 'treatment_posts' );

add_filter( 'gform_multiselect_placeholder_18', 'set_multiselect_placeholder', 10, 2 );

function set_multiselect_placeholder( $placeholder, $form_id ) {

    $lang = get_bloginfo("language"); 

    if ($lang == 'en-US') {
        return 'Choose the treatment';
    }else {
        return 'Behandlung wählen';
    }

   
}

function treatment_posts( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
        if ( $field->type != 'multiselect' || strpos( $field->cssClass, 'treatment-posts' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $args = array( 
            'suppress_filters' => false,
            'post_type' => 'treatment',
            'numberposts' => -1,
            'post_status'=> 'publish'

         );
        $posts = get_posts( $args );
 
        $choices = array();
 
        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_name );
        }
        $field->placeholder = 'Choose your treatment';
        $field->choices = $choices;
    }
 
    return $form;
}

// Dynamically Populating Multi-Select Fields with products posts 
add_filter( 'gform_pre_render_21', 'product_posts' );
add_filter( 'gform_pre_validation_21', 'product_posts' );
add_filter( 'gform_pre_submission_filter_21', 'product_posts' );
add_filter( 'gform_admin_pre_render_21', 'product_posts' );
add_filter( 'gform_multiselect_placeholder_21', 'set_multiselect_placeholder_product', 10, 2 );

function set_multiselect_placeholder_product( $placeholder, $form_id ) {
    $lang = get_bloginfo("language"); 

    if ($lang == 'en-US') {
        return 'Choose the product';
    }else {
        return 'Produkt wählen';
    }
}

function product_posts($form)
{
    foreach ($form['fields'] as &$field) {
        if ($field->type != 'multiselect' || strpos($field->cssClass, 'staticproduct') === false) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $args = array(
            'suppress_filters' => false,
            'post_type' => 'staticproduct',
            'numberposts' => -1,
            'post_status'=> 'publish'
         );
        $posts = get_posts($args);

        $choices = array();

        
        foreach ($posts as $post) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_name );
        }
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->choices = $choices;
    }

    return $form;
}


/**
 * @snippet  WooCommerce mini cart popup
 */


add_action('wp_ajax_ql_woocommerce_ajax_add_to_cart', 'ql_woocommerce_ajax_add_to_cart'); 
add_action('wp_ajax_nopriv_ql_woocommerce_ajax_add_to_cart', 'ql_woocommerce_ajax_add_to_cart');          

function ql_woocommerce_ajax_add_to_cart() {  

    $product_id = apply_filters('ql_woocommerce_add_to_cart_product_id', absint($_POST['product_id']));

    $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);

    $variation_id = absint($_POST['variation_id']);

    $passed_validation = apply_filters('ql_woocommerce_add_to_cart_validation', true, $product_id, $quantity);

    $product_status = get_post_status($product_id); 

    $cart_quantaty = WC()->cart->get_cart_contents_count();

    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) { 

        do_action('ql_woocommerce_ajax_added_to_cart', $product_id);

            if ('yes' === get_option('ql_woocommerce_cart_redirect_after_add')) { 

                wc_add_to_cart_message(array($product_id => $quantity), true); 

            } 

            WC_AJAX :: get_refreshed_fragments(); 

    } else { 

        $data = array( 

            'error' => true,

            'product_url' => apply_filters('ql_woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
        );

        echo wp_send_json($data);

    }

    wp_die();

}

function mode_theme_update_mini_cart() {
    woocommerce_mini_cart();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );

function custom_mini_cart() { 
    echo '<span class="dropdown-back" data-toggle="dropdown"> ';
    echo '<img src="' . get_template_directory_uri(). '/img/commerce.svg" alt="Cart" width="24" height="20">';
    echo '<div class="basket-item-count" style="display: inline;">';
        echo '<span class="cart-items-count count">';
            echo WC()->cart->get_cart_contents_count();
        echo '</span>';
    echo '</div>';
    echo '</span>';
    echo '<ul class="dropdown-menu dropdown-menu-mini-cart">';
        echo '<li><div class="widget_shopping_cart_content" id="mode-mini-cart">';
                  // woocommerce_mini_cart();
                mode_theme_update_mini_cart();
            echo '</div></li></ul>';

}
add_shortcode( 'custom-techno-mini-cart', 'custom_mini_cart' );

add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );

function iconic_cart_count_fragments( $fragments ) {
    
    $fragments['span.cart-items-count'] = '<span class="cart-items-count count">' . WC()->cart->get_cart_contents_count() . '</span>';
    
    return $fragments;
    
}


/*------------------------------------*\
    WooCommerce Custom Functions
\*------------------------------------*/

// 2. Trigger jQuery script
  
add_action( 'wp_footer', 'bbloomer_add_cart_quantity_plus_minus' );
  
function bbloomer_add_cart_quantity_plus_minus()
{
    // Only run this on the single product page
    if ( ! is_product() ) return;
}

/**
 * @snippet Automatically adding the product to the cart.
 */
add_filter( 'wp_ajax_nopriv_aaptc_add_product_to_cart', 'aaptc_add_product_to_cart' );
add_filter( 'wp_ajax_aaptc_add_product_to_cart', 'aaptc_add_product_to_cart' );
add_action( 'template_redirect', 'aaptc_add_product_to_cart', 10, 2 );
function aaptc_add_product_to_cart() {
    if( is_admin() || is_checkout() || WC()->cart->is_empty() )
        return;
    $targeted_products_ids = array(5445, 5446 );  // Product Id of the free product which will get added to cart
    $gift_products_ids = array(5697, 5590 );  // Product Id of the gift coupons
    $found                 = false;
    $gift                  = false;
    $num_of_items = WC()->cart->cart_contents_count;
    // error_log( 'MY STUFF: ' . print_r( $num_of_items, true ) );
    //check if product already in cart
    if ( $num_of_items >= 2 ) {
        foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
            $_product = $values['data'];
            // Check if gift exist in Cart
            if ( in_array($_product->get_id(), $gift_products_ids) ) {
                $gift = true;
            }
            // Check if free product exist in Cart
            if ( in_array($_product->get_id(), $targeted_products_ids) ) {
                $found = true;
            }
            // Remove free product by condition
            if ( $found && $num_of_items === 2 ) {
                WC()->cart->remove_cart_item( $cart_item_key );
            }
            if ( $found && $gift && $num_of_items === 3) {
                WC()->cart->remove_cart_item( $cart_item_key );
                $gift = false;
            }
        }
        if ( !$found ) {
            if ($gift && $num_of_items === 2) {
                // don't add free product
            } else {
                WC()->cart->add_to_cart( 5445 );
                WC()->cart->add_to_cart( 5446 );
            }
            // error_log( 'MY STUFF: ' . print_r( 'not found', true ) );
        }
    } else {
        foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
            $_product = $values['data'];
            if ( in_array($_product->get_id(), $targeted_products_ids) ) {
                WC()->cart->remove_cart_item( $cart_item_key );
            }
        }
    }
}

/**
* @snippet       Tiered Shipping Rates | WooCommerce
* @how-to        Get CustomizeWoo.com FREE
* @sourcecode    https://businessbloomer.com/?p=21425
* @author        Rodolfo Melogli
* @testedwith    WooCommerce 3.5.2
* @donate $9     https://businessbloomer.com/bloomer-armada/
*/
 
add_filter( 'woocommerce_package_rates', 'bbloomer_woocommerce_tiered_shipping', 10, 2 );
   
function bbloomer_woocommerce_tiered_shipping( $rates, $package ) {
    $gift_products_ids = array(5697, 5590 );  // Product Id of the gift coupons
   $num_of_items = WC()->cart->cart_contents_count;
   $threshold = 7;
   $gift = 0;
   foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
        $_product = $values['data'];
        // Check if gift exist in Cart
        if ( in_array($_product->get_id(), $gift_products_ids) ) {
            $gift = true;
        }
    }

    if ( $num_of_items === 1 && $gift) {
        unset( $rates['flat_rate:1'] );
        unset( $rates['flat_rate:3'] );

    } elseif ( $num_of_items <= ( $threshold - $gift) ) {
        unset( $rates['flat_rate:3'] );
        unset( $rates['flat_rate:4'] );

    } else {
      unset( $rates['flat_rate:1'] );
      unset( $rates['flat_rate:4'] );
    }
   
   return $rates;
   
}

// Remove button that removes item from product
add_filter('woocommerce_cart_item_remove_link', 'customized_cart_item_remove_link', 20, 2 );
function customized_cart_item_remove_link( $button_link, $cart_item_key ){
     if( is_page( 'cart' ) || is_cart() ) {
        // return;
    //SET HERE your specific products IDs
    $targeted_products_ids = array(5445, 5446);

    // Get the current cart item
    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    // If the targeted product is in cart we remove the button link
    if( in_array($cart_item['data']->get_id(), $targeted_products_ids) )
        $button_link = '';

	}
    return $button_link;
}

// Remove quantity input from product
add_filter( 'woocommerce_cart_item_quantity', 'wc_cart_item_quantity', 10, 3 );
function wc_cart_item_quantity( $product_quantity, $cart_item_key, $cart_item ){
    $targeted_products_ids = array(5445, 5446 );

     // Get the current cart item
    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    // If the targeted product is in cart we remove the button link
    if( in_array($cart_item['data']->get_id(), $targeted_products_ids) )
        $product_quantity = '';
    return $product_quantity;
}

function excerpt_in_cart($item_name, $cart_item, $cart_item_key) {
    global $_product;

    $targeted_products_ids = array(5445, 5446 );

    
    if( in_array( $cart_item['data']->get_id(), $targeted_products_ids ) ) {
        $excerpt = get_the_excerpt($cart_item['product_id']);
        $item_name .=  '<br><small class="shortDescription">' . $excerpt .  '</small>';
    }
    return $item_name;

}
add_filter('woocommerce_cart_item_name', 'excerpt_in_cart', 99, 3);

// Cart item link
add_filter( 'woocommerce_cart_item_name', 'conditionally_remove_link_from_cart_item_name', 10, 3 );
function conditionally_remove_link_from_cart_item_name( $item_name, $cart_item, $cart_item_key ) {
    // HERE set your Free product ID
    $targeted_products_ids = array(5445, 5446 );

    if ( in_array($cart_item['data']->get_id(), $targeted_products_ids) ) {
        $item_name = $cart_item['data']->get_name();
    }
    return $item_name;
}

// Mini-cart item link
add_filter( 'woocommerce_cart_item_permalink', 'conditionally_remove_cart_item_permalink', 10, 3 );
function conditionally_remove_cart_item_permalink( $permalink, $cart_item, $cart_item_key ) {
    // HERE set your Free product ID
    $targeted_products_ids = array(5445, 5446 );

    if ( in_array($cart_item['data']->get_id(), $targeted_products_ids) ) {
        $permalink = '#';
    }
    return $permalink;
}

// Order item link
add_filter( 'woocommerce_order_item_name', 'conditionally_remove_link_from_order_item_name', 10, 2 );
function conditionally_remove_link_from_order_item_name( $item_name, $item ) {
    // if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) )
    // HERE set your Free product ID
    $targeted_products_ids = array(5445, 5446 );

    if ( in_array($item->get_product_id(), $targeted_products_ids) ) {
        $item_name = $item->get_name();
    }
    return $item_name;
}
// *************
// THIS IS JUST FOR TESTING PURPOSES!!!
// *************

add_action( 'woocommerce_before_cart', 'bbloomer_find_product_in_cart' );
function bbloomer_find_product_in_cart($rates) {
   $threshold = 6;
    
//    if ( WC()->cart->cart_contents_count < $threshold ) {
// //          unset( $rates['flat_rate:1'] );
// 	   echo( '< 5');
//    } else {
// //       unset( $rates['flat_rate:3'] );
// 	   echo( '> 5');
//    }
   
//    return $rates;
//    $product_id = 5445;
//    $in_cart = false;
  
//    foreach( WC()->cart->get_cart() as $cart_item ) {
//       $product_in_cart = $cart_item['product_id'];
//       if ( $product_in_cart === $product_id ) $in_cart = true;
//    }
//   $targeted_products_ids = array(5445, 5446);

    // Get the current cart item
//     $cart_item = WC()->cart->get_cart()[$cart_item_key];
// 	print_r($targeted_products_ids);
// 	print_r($cart_item['data']);
// 	wc_print_notice( $targeted_products_ids, 'notice' );
// 	error_log(print_r($targeted_products_ids, true));
    // If the targeted product is in cart we remove the button link
//     if( in_array($cart_item['data']->get_id(), $targeted_products_ids) )
      
   // if ( $in_cart ) {
  
      // $notice = 'Product ID ' . $in_cart . ' is in the Cart!';
      // wc_print_notice( $notice, 'notice' );
      // print_r($in_cart);
      // print_r(WC()->cart->cart_contents_count);
//       $targeted_products_ids = 5446;
//       echo in_array($targeted_products_ids, WC()->cart->get_cart_item_quantities());
//       print_r(WC()->cart->get_cart_item_quantities());
  
   // }
  
}
// *************
// TEST END!!!
// *************

// Create shortcode banner 
// function that runs when shortcode is called
function wpb_banner_shortcode() {
    return '
        <div class="single-banner">
            <div class="banner-content">
                <img class="banner-content-shape" src="' . get_template_directory_uri() . '/img/banner-image-shape.png" width="122" height="116" alt="Myesthetic">
                <div class="banner-content-inner">
                    '.get_field('promotion_text', 'option').'
                </div>
            </div>
            <div class="banner-image">
                <div class="banner-main-image">
                    <img class="banner-main-inner-image" src="' . get_template_directory_uri() . '/img/banner-image.png" width="386" height="334" alt="Myesthetic">
                    <img class="banner-shape-right" src="' . get_template_directory_uri() . '/img/banner-shaperight.png" width="293" height="182" alt="Myesthetic">
                    <img class="banner-shape-left" src="' . get_template_directory_uri() . '/img/banner-shapeleft.png" width="339" height="162" alt="Myesthetic">
                </div>
            </div>
        </div>
    ';
} 

// register shortcode
add_shortcode('promo_banner', 'wpb_banner_shortcode'); 

function wpbeginner_remove_version() {
    return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');


/**
 * @snippet       Disable WooCommerce Ajax Cart Fragments On Static Homepage
 */
 
// add_action( 'wp_enqueue_scripts', 'bbloomer_disable_woocommerce_cart_fragments', 11 ); 
 
function bbloomer_disable_woocommerce_cart_fragments() { 
   if ( is_front_page() ) wp_dequeue_script( 'wc-cart-fragments' ); 
}

// Conditionally redirect users after add to cart for certain product
function my_custom_add_to_cart_redirect( $url ) {
    
    if ( ! isset( $_REQUEST['add-to-cart'] ) || ! is_numeric( $_REQUEST['add-to-cart'] ) ) {
        return $url;
    }
    
    $product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_REQUEST['add-to-cart'] ) );
    
    // Only redirect the product IDs in the array to the checkout
    if ( in_array( $product_id, array( 5697, 5590) ) ) {
        $url = WC()->cart->get_checkout_url();
    }
    
    return $url;

}


// -----------------------------------------
// 1. Show custom input field above Add to Cart
 
function bbloomer_product_add_on_1() {
    global $product;
    if ( 5697 === $product->get_id() || 5590 === $product->get_id() ) {

        echo '<p class="section-form">' . __( 'Für', 'myesthetic' ) . '</p>';

        $value = isset( $_POST['gift_name_surname'] ) ? sanitize_text_field( $_POST['gift_name_surname'] ) : '';
        echo '<div class="form-wrap"><input name="gift_name_surname" placeholder="' . __( 'Name und Nachname', 'myesthetic' ) . '" value="' . $value . '" type="text" required></div>';
    }
}
// -----------------------------------------
// 1.1. Show custom input field above Add to Cart
 
function bbloomer_product_add_on_1_1() {
    global $product;
    if ( 5697 === $product->get_id() || 5590 === $product->get_id() ) {

        $value = isset($_POST['gift_checkbox_pp']) ? 1 : 0;
        echo '<label for="gift_checkbox_pp" class="consent">';
        echo '<input id="gift_checkbox_pp" type="checkbox" name="gift_checkbox_pp" value="' . $value . '" required>';
        echo __( '  Mit der Nutzung dieses Formulars erklären Sie sich mit der Speicherung und Verarbeitung Ihrer Daten durch diese Website einverstanden ', 'myesthetic' );
        echo '<a href="/datenschutz-bestimmungen/">' . __( 'Datenschutz-bestimmungen', 'myesthetic' ) . '</a></label><br>';
    }
}

 
// -----------------------------------------
// 2. Throw error if custom input field empty
 
function bbloomer_product_add_on_validation( $passed, $product_id, $qty ){
    
   if ( 5697 === $product_id || 5590 === $product_id ) {
       if( isset( $_POST['gift_name_surname'] ) && sanitize_text_field( $_POST['gift_name_surname'] ) == '' ) {
          wc_add_notice( 'Rechnung: Name und Nachname ist ein Pflichtfeld.', 'error' );
          $passed = false;
       }
       if( isset( $_POST['gift_checkbox_pp'] ) && sanitize_text_field( $_POST['gift_checkbox_pp'] ) == '' ) {
          wc_add_notice( 'Rechnung: Kontrollkästchen ist ein Pflichtfeld.', 'error' );
          $passed = false;
       }
       return $passed;
   }
}
 
// -----------------------------------------
// 3. Save custom input field value into cart item data
 
function bbloomer_product_add_on_cart_item_data( $cart_item_data ){
    if( isset( $_POST['gift_name_surname'] ) ) {
        $cart_item_data['gift_name_surname'] = sanitize_text_field( $_POST['gift_name_surname'] );
    }
    return $cart_item_data;

}
 
// -----------------------------------------
// 4. Display custom input field value @ Cart
 
function bbloomer_product_add_on_display_cart( $item_data, $cart_item_data  ) {
    if ( isset( $cart_item_data['gift_name_surname'] ) ){
        $item_data[] = array(
             'key' => __( 'für', 'myesthetic' ),
             'value' => wc_clean( $cart_item_data['gift_name_surname'] )
        );
    }
    return $item_data;

}

// -----------------------------------------
// 5. Save custom input field value into order item meta
 
function bbloomer_product_add_on_order_item_meta( $item_id, $values ) {
    if ( ! empty( $values['gift_name_surname'] ) ) {
        wc_add_order_item_meta( $item_id, 'Für', $values['gift_name_surname'], true );
    }
}
 
add_filter( 'woocommerce_add_to_cart_redirect', 'my_custom_add_to_cart_redirect' );
add_action( 'woocommerce_before_add_to_cart_button', 'bbloomer_product_add_on_1', 9 );
add_action( 'woocommerce_before_add_to_cart_button', 'bbloomer_product_add_on_1_1', 11 );
add_filter( 'woocommerce_add_to_cart_validation', 'bbloomer_product_add_on_validation', 10, 3 );
add_filter( 'woocommerce_add_cart_item_data', 'bbloomer_product_add_on_cart_item_data', 10, 2 );
add_filter( 'woocommerce_get_item_data', 'bbloomer_product_add_on_display_cart', 10, 2 );
add_action( 'woocommerce_add_order_item_meta', 'bbloomer_product_add_on_order_item_meta', 10, 2 );


add_action( 'woocommerce_cart_coupon', 'custom_woocommerce_empty_cart_button' );
function custom_woocommerce_empty_cart_button() {
    echo '<a href="' . esc_url( add_query_arg( 'empty_cart', 'yes' ) ) . '" style="background-color: transparent!important;" class="button" title="' . esc_attr( 'leerer Warenkorb', 'myesthetic' ) . '">' . esc_html( 'leerer Warenkorb', 'myesthetic' ) . '</a>';
}

add_action( 'wp_loaded', 'custom_woocommerce_empty_cart_action', 20 );
function custom_woocommerce_empty_cart_action() {
    if ( isset( $_GET['empty_cart'] ) && 'yes' === esc_html( $_GET['empty_cart'] ) ) {
        WC()->cart->empty_cart();

        $referer  = wp_get_referer() ? esc_url( remove_query_arg( 'empty_cart' ) ) : wc_get_cart_url();
        wp_safe_redirect( $referer );
    }
}

add_filter('body_class','add_category_to_single');
  function add_category_to_single($classes) {
    if (is_single() ) {
      global $post;
      foreach((get_the_category($post->ID)) as $category) {
        
        $term_id = $category->term_id;
        if($term_id == 104 || $term_id == 106 ) {
            $classes[] = 'orange-header';
        }
      }
    }
    // return the $classes array
    return $classes;
  }

// Remove JQuery migrate
function remove_jquery_migrate( $scripts ) {
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        if ($script->deps) { 
            // Check whether the script has any dependencies
            $script->deps = array_diff( $script->deps, ['jquery-migrate']);
        }
    }
}
add_action('wp_default_scripts', 'remove_jquery_migrate');


// function that runs when shortcode is called
function wpb_custom_block_shortcode($id) { 
    require('template-parts/custom-block-shortcode.php');
} 

// register shortcode
add_shortcode('custom_block', 'wpb_custom_block_shortcode'); 

// function that runs when shortcode is called
function wpb_promo_treatment_shortcode($id) { 
    require('template-parts/promo-treatment-shortcode.php');
} 

// register shortcode
add_shortcode('promo_treatment', 'wpb_promo_treatment_shortcode'); 

// function that runs when shortcode is called
function wpb_promo_product_shortcode($id) { 
    require('template-parts/promo-product-shortcode.php');
} 

// register shortcode
add_shortcode('promo_product', 'wpb_promo_product_shortcode'); 