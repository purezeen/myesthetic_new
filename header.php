<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="theme-color" content="#323231"/>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
<!-- Adobe fonts  -->
<link rel="stylesheet" href="https://use.typekit.net/slw1yzd.css">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-sduri="<?php echo get_stylesheet_directory_uri(); ?>">

<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
		<div class="header-wrap">
			<div class="burger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>

			<div class="site-branding">
				<?php
					$my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
				?>

				<a href="<?php echo $my_home_url; ?>" aria-label="myesthetic" class="site-logo">
					<!-- <img class="logo-dark" src="<?php //echo get_template_directory_uri() ?>/img/myesthetic-logo-header-resized.png" alt="my-esthatic" width="330" height="72"> -->
					<img class="logo-dark" src="<?php echo get_template_directory_uri() ?>/img/logo-header.png" width="110" height="20" alt="my-esthatic">

					<!-- <img class="logo-white" src="<?php //echo get_template_directory_uri() ?>/img/myesthetic-logo-white-resized.png" alt="my-esthatic" width="110" height="20"> -->
					<img class="logo-white" src="<?php echo get_template_directory_uri() ?>/img/myesthetic-logo-whitex2.png" alt="my-esthatic" width="110" height="20">

				</a>

				<div class="phone">
					<p><span class="separator">|</span><?php the_field( 'mobile_number', 'option' ); ?></p>
					<div class="show-phone-details">
					<div class="phone-wrap">
						<a href="https://api.whatsapp.com/send?phone=16094776168" rel="noreferrer" target="_blank" class="social-icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/whatsup-dark.svg" alt="whatsup" width="26" height="25">
							<span>whats up</span>
						</a>

						<a href="tel:<?php the_field( 'mobile_number', 'option' ); ?>" rel="noreferrer" class="social-icon">
							<img src="<?php echo get_template_directory_uri() ?>/img/call-dark.svg" alt="phone" width="26" height="25">
							<span>phone</span>
						</a>
					</div>
					</div>

				</div>
			</div>

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav>
			
			<div class="nav-right">
				<div class="cart-wrap">
					<a href="<?php echo $my_home_url; ?>/my-account/" aria-label="<?php _e( 'My account', 'myesthetic' ); ?>"><span class="account-title"><?php _e( 'My account', 'myesthetic' ); ?></span><span class="account-icon"><img src="<?php echo get_template_directory_uri() ?>/img/account-icon.svg" alt="<?php _e( 'My account', 'myesthetic' ); ?>" width="24" height="24"></span></a>
					<div class="mini-cart-wrap">
					
						<div>
						<span class="cart-close">+</span>
					<?php 
					echo do_shortcode( '[custom-techno-mini-cart]' ); 
					?>
					</div>
					</div>
				</div>
				<?php do_action('wpml_add_language_selector'); ?>
			</div>
			
			
		</div>
		</div>

	
	</header><!-- #masthead -->

	<div id="content" class="site-content">
