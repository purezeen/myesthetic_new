<?php /* Template Name: HomePage */

get_header(); ?>

<!-- Home page header  -->
<section class="container header home-header">
   <div class="header-image">

      <?php $hero_image = get_field( 'hero_image' ); ?>

      <?php if ( $hero_image ) { ?>
        <img src="<?php echo $hero_image['sizes']['medium']; ?>"
            srcset="<?php echo $hero_image['sizes']['image_tablet']; ?> 800w, <?php echo $hero_image['sizes']['medium']; ?> 490w"
            sizes="(max-width: 1265px) 1265px, 100vw"
            alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>"
            width="<?php echo $hero_image['sizes']['image_tablet-width']; ?>"
            height="<?php echo $hero_image['sizes']['image_tablet-height']; ?>" />
      <?php }?>

   </div>
   <div class="header-content">
      <?php the_field( 'hero_content' ); ?>

      <?php if ( have_rows( 'hero_button' ) ) : ?>

        <?php while ( have_rows( 'hero_button' ) ) : the_row(); ?>

            <?php if ( get_sub_field( 'button_url' ) == 1 ) { ?>
                <a href="<?php the_sub_field( 'custom_url' ); ?>" target="_blank" class="btn btn-full btn-green btn-arrow"><?php the_sub_field( 'button_text' ); ?></a>
            <?php } else { ?>
            
            <?php $post_object = get_sub_field( 'button_link_to_page' ); ?>

            <?php if ( $post_object ): ?>

                <?php $post = $post_object; ?>
                <?php setup_postdata( $post ); ?>
                <a href="<?php the_permalink(); ?>" class="btn btn-full btn-green btn-arrow"><?php the_sub_field( 'button_text' ); ?></a>
                <?php wp_reset_postdata(); ?>

            <?php endif; ?>
            <?php } ?>

        <?php endwhile; ?>

      <?php endif; ?>

   </div>
</section>

<!-- Number counter  -->
<section class="container number-counter section" id="number-counter">

    <h2 class="number-counter-title"><?php _e('Let numbers speak for us', 'myesthetic') ?></h2>

    <?php if ( have_rows( 'number_counter_list' ) ) : ?>
        <div class="number-wrap">
            <?php while ( have_rows( 'number_counter_list' ) ) : the_row(); ?>
                <div>
                    <h2 class="count-wrap"> <?php the_sub_field( 'number' ); ?></h2>
                    <hr>
                    <span><?php the_sub_field( 'number_title' ); ?></span>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

</section>

<!-- Benefit  -->
<section class="benefit benefit-orange clearfix">
   <div class="benefit-title">
      <?php the_field( 'benefits_content' ); ?>
   </div>
   <?php if ( have_rows( 'benefits_list' ) ) : ?>
        <div class="slick-benefit-wrap">
            <div class="slick-benefit ">
                <?php $counter = 1; while ( have_rows( 'benefits_list' ) ) : the_row(); ?>
                <div>
                    <div class="benefit-box">

                    <?php $icon = get_sub_field( 'icon' ); ?>
                    <?php if ( $icon ) { ?>
                    <div class="benefit-icon">
                        <!-- <img src="<?php //echo $icon['sizes']['thumbnail']; ?>" 
                            alt="<?php //_e('my-esthetic', 'myesthetic'); ?>-<?php //echo $icon['alt']; ?>" 
                            width="<?php //echo $hero_image['sizes']['thumbnail-width']; ?>"
                            height="<?php //echo $hero_image['sizes']['thumbnail-height']; ?>"
                        /> -->
                        <?php echo $counter++; ?>
                    </div>
                    <?php } ?>

                    <div class="h6"><?php the_sub_field( 'title' ); ?></div>

                    <p><?php the_sub_field( 'content' ); ?></p>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
   <?php endif; ?>

   <!-- <div class="benefit-shape">
        <img src="<?php //echo get_template_directory_uri() ?>/img/benefit-icon.svg" alt="Benefit icon" width="289"
            height="159">
    </div> -->
</section>

<!-- Gallery  -->
<?php if ( have_rows( 'gallery' ) ) : ?>

   <section class="container section gallery-team pl-sm-0 pr-sm-0">
      <div class="gallery-team__intro">
         <div>
            <h2><?php the_field( 'gallery_title' ); ?></h2>
            <?php the_field( 'description' ); ?>
            <a href="<?php _e( '/en/about-us', 'myesthetic' ); ?>/"
               class="btn btn-full btn-green btn-arrow"><?php _e( 'About us', 'myesthetic' ); ?></a>
         </div>
      </div>
      <?php $i=1; ?>
      <?php while ( have_rows( 'gallery' ) ) : the_row(); ?>
         <?php $image = get_sub_field( 'image' ); ?>
         <?php if ( $image ) { ?>
            <a class="gallery-item gallery-team__item-<?php echo $i; ?>" href="<?php echo $image['sizes']['image_tablet']; ?>"
               data-rel="lightcase:myCollection">
               <img class="gallery-img" 
               srcset=" <?php echo $image['sizes']['image_tablet']; ?> 490w, <?php echo $image['sizes']['large']; ?> 800w"
               sizes="(min-width: 1265px) 1265px, 100vw"
               alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $image['alt']; ?>"
               width="<?php echo $hero_image['sizes']['large-width']; ?>"
               height="<?php echo $hero_image['sizes']['large-height']; ?>" />
            </a>
         <?php } ?>
      <?php $i++; ?>
      <?php endwhile; ?>   

   </section>

<?php endif; ?>

<!-- Associate  -->
<?php if ( have_rows( 'associate' ) ) : ?>

   <section class="container associate">

      <h2 class="associate__title"><?php the_field( 'section_title_associate' ); ?></h2>

      <?php get_template_part('template-parts/associate', 'list'); ?>

   </section>

<?php endif; ?>

<!-- Shortcode  -->
<section class="container shortcode">
   <?php the_field( 'add_shortcode' ); ?>
</section>

<!-- Testimonials  -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBR4M8XbKC0okrDwwkMrj-onMLbpIk1oAI">
</script>
<section class="container testimonials testimonials-white section pl-sm-0 pr-sm-0">
   <div class="testimonial-title">
      <h2><?php _e('Our customers reviews', 'myesthetic') ?></h2>
      <a href="ttps://www.google.com/search?client=firefox-b-d&q=myesthetic#lrd=0x47b8cah3e86136301:0xc309be6086ea6b0c,1,,,"
         rel="noreferrer" class="btn btn-arrow btn-arrow-green btn-arrow-small"
         target="_blank"><?php _e('Read all Google reviews', 'myesthetic') ?></a>
   </div>

   <?php if( ICL_LANGUAGE_CODE == 'en' ){ ?>
   <?php if ( have_rows( 'testimonial' ) ) : ?>
   <div class="slick-testimonials">

      <?php while ( have_rows( 'testimonial' ) ) : the_row(); ?>

      <div class="slick-slide">
         <div class="testimonial-content">
            <span class="quote"></span>
            <blockquote class="content-wrap">
               <?php the_sub_field( 'testimonial_content' ); ?>
            </blockquote>
         </div>

         <div class="testimonial-info">
            <div class="h6"> <?php the_sub_field( 'testimonial_name' ); ?><span class="review-sep"></span></div>

            <span>
               <div class="review-stars">

                  <?php $ratingstar = get_sub_field( 'number_of_stars' ); ?>
                  <?php
                        if($ratingstar == 1 ) {
                           $star_class= 'onestar';
                        }else if($ratingstar == 2) {
                           $star_class= 'twostar';
                        }
                        else if($ratingstar == 3) {
                           $star_class= 'threestar';
                        }
                        else if($ratingstar == 4) {
                           $star_class= 'fourstar';
                        }
                        else if($ratingstar == 5) {
                           $star_class= 'fivestar';
                        }
                        ?>
                  <ul class="<?php echo $star_class; ?>">
                     <li><i class="star"></i></li>
                     <li><i class="star"></i></li>
                     <li><i class="star"></i></li>
                     <li><i class="star"></i></li>
                     <li><i class="star"></i></li>
                  </ul>
               </div>
            </span>
         </div>
      </div>
      <?php endwhile; ?>
      <?php else : ?>
      <?php // no rows found ?>
      <?php endif; ?>
   </div>
   <?php
   } else { ?>
   <div class="slick-testimonials" id="google-reviews">
      <!-- Load testimonials from Google Comments -->
   </div>
   <?php } ?>
</section>
<!-- Testimonials end -->

<!-- Products -->
<?php if ( have_rows( 'product_box' ) ) : ?>
    <section class="container section products-intro" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/woman-products-bg.png')">
        <div class="products-intro__title">
            <h2><?php the_field( 'section_title' ); ?></h2>
            <a href="<?php _e( '/en/products/', 'myesthetic' ); ?>/" class="btn btn-arrow btn-arrow-green"><?php _e( 'See all products', 'myesthetic' ); ?></a>
        </div>

        <?php while ( have_rows( 'product_box' ) ) : the_row(); ?>

            <?php $choose_the_product_term = get_sub_field( 'choose_the_product' ); ?>
            <?php if ( $choose_the_product_term ): ?>
               <?php $product_catname = $choose_the_product_term->name; ?>
               <?php $product_catname = strtolower($product_catname); $product_catname = str_replace(' ', '', $product_catname);?>
            <?php endif; ?>

            <?php $product_button = get_sub_field( 'product_button' ); ?>
            <a href="<?php echo $product_button['url']; echo "#" . $product_catname; ?>" class="products-intro__box">

                <?php $product_logo = get_sub_field( 'product_logo' ); ?>

                <?php if ( $product_logo ) { ?>
                  <div class="products-intro__img">
                     <img src="<?php echo $product_logo['sizes']['medium'];  ?>" alt="<?php echo $product_logo['alt']; ?>" height="<?php echo $product_logo['sizes']['medium-height']?>" width="<?php echo $product_logo['sizes']['medium-width']?>"/>
                  </div>
                 <?php } ?>
                <p><?php the_sub_field( 'product_description' ); ?></p>
                   
               <button class="btn btn-full btn-green" target="<?php echo $product_button['target']; ?>"><?php echo $product_button['title']; ?></button>
            </a>

        <?php endwhile; ?>

    </section>
<?php endif; ?>

<!-- Sponsors  -->
<?php if ( have_rows( 'sponsor' ) ) : ?>
   <section class="container partners section">

      <?php while ( have_rows( 'sponsor' ) ) : the_row(); ?>
      <?php $sponsor_picture = get_sub_field( 'sponsor_picture' ); ?>
      <?php if ( $sponsor_picture ) { ?>

      <a href=" <?php the_sub_field( 'sponsor_link' ); ?>" aria-label="partner-logo" rel="noreferrer" rel="nofollow"
         target="_blank" class="partner-logo">
         <img src="<?php echo $sponsor_picture['sizes']['medium']; ?>"
            alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $sponsor_picture['alt']; ?>" width="300"
            height="100" />
      </a>

      <?php } ?>
      <?php endwhile; ?>

   </section>
<?php endif; ?>

<?php get_footer(); ?>