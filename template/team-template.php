<?php /* Template Name: Team */ get_header(); ?>

<!-- Home page header  -->
<section class="container header team-header">
   <div class="header-image">
      <?php if (has_post_thumbnail()) {
         the_post_thumbnail($size = 'image_tablet');
      } ?>
   </div>
   <div class="header-content">
      <?php the_content(); ?>
   </div>
</section>

<?php $args = array(
   'post_type' => 'team',
   'posts_per_page' => -1,
   'post_status' => 'publish'
 );
?>

<?php $the_query = new WP_Query( $args ); ?>

<?php if ($the_query->have_posts()) :  ?>
   <section class="container team">
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

         <?php if ( get_field( 'disable_single_page' ) == 1 ) { 
            $user_link_open = '<div class="team-box">';
            $user_link_close = "</div>";
         } else { 
            $user_link_open = '<a href="' . get_permalink() . '" class="team-box" >';
            $user_link_close = "</a>";
         } ?>

         <?php echo $user_link_open; ?>
            <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
            <div class="team-image cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>)">
            </div>

            <div class="team-content">
               
               <h2><?php the_title(); ?></h2>
               <p class="subtitle"><?php the_field( 'job_title' ); ?></p>
               <div class="team-content-description">

                  <?php $team_description = get_the_content(); ?>
                  
                  <?php if ( get_field( 'disable_single_page' ) == 1 ) { 
                     echo $team_description;
                  } else { ?>
                     <?php $x = 70; ?>
                     <?php $parts = explode(' ',$team_description );
                     if (sizeof($parts)>$x) {
                        $parts = array_slice($parts,0,$x);
                        $dots = "...";
                     }else {
                        $dots = "";
                     } ?>
                    <p><?php  echo implode(' ',$parts); ?><?php echo $dots; ?></p>
                  <?php } ?>
               </div>

               <?php if ( get_field( 'disable_single_page' ) == 0 ) { ?>
                  <div class="btn btn-border btn-arrow"><?php _e('Read more', 'myesthetic'); ?></div>
               <?php } ?>
            </div>
         <?php echo $user_link_close; ?>

      <?php endwhile; ?>
   </section>
<?php endif; ?>

<?php get_footer(); ?>