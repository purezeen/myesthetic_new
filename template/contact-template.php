<?php /* Template Name: Contact */

get_header(); ?>

<section class="contact-page contact-layout container">
   <div class="contact-main">
      <?php the_content(); ?>



      <div class="contact-form">
         <ul id="tabs" class="tabs">

            <li><a href="#freeconsultation" class="freeconsultation"><?php _e( 'Free consultation', 'myesthetic' ); ?></a></li>
            <li><a href="#treatments" class="treatments"><?php _e( 'Treatments', 'myesthetic' ); ?></a></li>
            <li><a href="#products" class="products"><?php _e( 'Products', 'myesthetic' ); ?></a></li>
            <li><a href="#jobenquiry" class="jobenquiry"><?php _e( 'Job enquiry', 'myesthetic' ); ?></a></li>
            <li><a href="#other" class="other"><?php _e( 'Other', 'myesthetic' ); ?></a></li>

         </ul>
         <div class="tabwrap" id="freeconsultation">
            <div class="form-wrap">
               <?php gravity_form( 17 , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true,  $echo = true ); ?>
            </div>
         </div>
         <div class="tabwrap" id="treatments">
            <div class="form-wrap">
               <?php gravity_form( 18 , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true,  $echo = true ); ?>
            </div>
         </div>
         <div class="tabwrap" id="products">
            <div class="form-wrap">
               <?php gravity_form( 21 , $display_title = false, $display_description = false, $display_inactive = true, $field_values = null, $ajax = true, $echo = true ); ?>
            </div>
      
         </div>
         <div class="tabwrap" id="jobenquiry">
            <div class="form-wrap">
               <?php gravity_form( 19 , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true,  $echo = true ); ?>
            </div>
         </div>
         <div class="tabwrap" id="other">
            <div class="form-wrap">
               <?php gravity_form( 20 , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false,  $echo = true ); ?>
            </div>
         </div>
      </div>
   </div>
   <div class="contact-side">
      <div class="contact-map">
         <div class="contact-map-wrap contact-map-wrap-img">
            <div id="map" class="map mapboxgl-map"></div>
         </div>

         <form class="map-direction-wrap" action="http://maps.google.com/maps" method="get" target="_blank">
            <p><?php _e( 'Find directions from your starting location', 'myesthetic' ); ?></p>
            <input type="text" name="saddr" placeholder="<?php _e( 'Enter your starting adress', 'myesthetic' ); ?>" />
            <input type="hidden" name="daddr" value="Königsallee 61, 40215 Düsseldorf, Deutschland" />
            <div class="button-arrow">
            <input class="btn" type="submit" value="<?php _e( 'get directions', 'myesthetic' ); ?>" />
            <img src="<?php echo get_template_directory_uri() ?>/img/btn-arrow-green.svg" width="23" height="9" alt="<?php _e('my-esthetic', 'myesthetic'); ?>" >
            </div>
         </form>
      </div>

      <div class="contact-info">
         <ul class="unstyle-list">
            <?php if ( have_rows( 'street_address', 'option' ) ) : ?>
            <?php while ( have_rows( 'street_address', 'option' ) ) : the_row(); ?>
            <li><a href="<?php the_sub_field( 'street_address_url' ); ?>"
                  target="_blank"><?php the_sub_field( 'street_address_name' ); ?></a></li>
            <?php endwhile; ?>
            <?php endif; ?>
            <li><a href="tel:<?php the_field( 'mobile_number', 'option' ); ?>">m
                  <?php the_field( 'mobile_number', 'option' ); ?></a></li>
            <li><a href="tel:<?php the_field( 'telephone', 'option' ); ?>"> t
                  <?php the_field( 'telephone', 'option' ); ?></a></li>
            <li><a href="mailto:<?php the_field( 'email', 'option' ); ?>"><?php the_field( 'email', 'option' ); ?></a>
            </li>
         </ul>
      </div>
   </div>
</section>

<?php get_footer(); ?>