<?php /* Template Name: Shop */
get_header(); ?>


<section class="container coupon-header">
  <div class="header-image">

  <?php $hero_image = get_field( 'hero_image' ); ?>
   <?php if ( $hero_image ) { ?>
      <div class="header-image-wrap">
         <img src="<?php echo $hero_image['sizes']['medium_large']; ?>" 
            alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>"
            width="<?php echo $hero_image['sizes']['medium_large-width']; ?>"
            height="<?php echo $hero_image['sizes']['medium_large-height']; ?>"
         />
     </div>
   <?php }else if(has_post_thumbnail()){
     $thumb_id = get_post_thumbnail_id();
      ?>
      <div class="header-image-wrap">

      <img src="<?php 
         $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium-large', false);
         echo $thumb_url = $thumb_url_array[0]; ?>" srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium_large', false);
         echo $thumb_url_array_medium[0]; ?> 490w" sizes="(max-width: 1265px) 1265px, 100vw" 
         alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
         width="<?php echo $thumb_url_array[1]; ?>"
         height="<?php echo $thumb_url_array[2]; ?>"
      />
     </div>
   <?php
   } ?>

  </div>
  <div class="header-content">
      <?php the_field( 'hero_content' ); ?>
  </div>
</section>

<section class="coupon-layout container">
   <div class="coupon-main">
      <h1><?php _e( 'Gift coupon', 'myesthetic' ); ?></h1>

      <div class="contact-form form-wrap">
        <?php the_content(); ?>
      </div>
   </div>
   <div class="coupon-side">
     
      <div class="contact-info">
         <ul class="unstyle-list">
            <li>
            <?php if ( have_rows( 'street_address', 'option' ) ) : ?>
               <?php while ( have_rows( 'street_address', 'option' ) ) : the_row(); ?>
                  <a href="<?php the_sub_field( 'street_address_url' ); ?>" target="_blank"><?php the_sub_field( 'street_address_name' ); ?></a>
               <?php endwhile; ?>
            <?php endif; ?>
            <li><a href="tel:<?php the_field( 'mobile_number', 'option' ); ?>">m <?php the_field( 'mobile_number', 'option' ); ?></a></li>
            <li><a href="tel:<?php the_field( 'telephone', 'option' ); ?>"> t <?php the_field( 'telephone', 'option' ); ?></a></li>
            <li><a href="mailto:<?php the_field( 'email', 'option' ); ?>"><?php the_field( 'email', 'option' ); ?></a></li>
         </ul>
      </div>
   </div>
</section>

<section class="container section editor">
  <?php get_template_part('template-parts/flexible', 'content');?>
</section>

<?php get_footer(); ?>