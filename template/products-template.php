<?php /* Template Name: Products */ get_header(); ?>

 <?php if ( get_field( 'show_promotion', 'option' ) == 1 ) { 
    $header_promo = "header-promo";
  } ?>

<section class="container product-header <?php echo $header_promo; ?>">
  <div class="header-image">
    <?php $hero_image = get_field( 'hero_image' ); ?>
    <?php if ( $hero_image ) { ?>
      <img src="<?php echo $hero_image['sizes']['image_tablet']; ?>" 
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" 
        width="<?php echo $hero_image['sizes']['image_tablet-width']; ?>"
        height="<?php echo $hero_image['sizes']['image_tablet-height']; ?>" />
    <?php } else if (has_post_thumbnail()) { ?>
      <?php $thumb_id = get_post_thumbnail_id(); ?>
        <img src="<?php $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', false);
          echo $thumb_url = $thumb_url_array[0]; ?>" 
          srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
          echo $thumb_url_array_medium[0]; ?> 490w " sizes="(max-width: 1265px) 1265px, 100vw" 
          alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
          width="<?php echo $thumb_url_array[1]; ?>"
          height="<?php echo $thumb_url_array[2]; ?>" />
    <?php } ?>
  </div>
   
  <div class="header-content">
    <?php the_field( 'hero_content' ); ?>

    <?php if ( get_field( 'show_promotion', 'option' ) == 1 ) { ?>
      <div class="header-promo-wrap">
        <div class="header-promo-banner">
          <?php the_field( 'promotion_text', 'option' ); ?>
        </div>
      </div>
		<?php } ?>

  </div>

  <?php $hero_mobile_icon = get_field( 'hero_mobile_icon' ); ?>
  <?php if ( $hero_mobile_icon ) { ?>
    <div class="product-icon">
      <img src="<?php echo $hero_mobile_icon['sizes']['thumbnail']; ?>" 
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_mobile_icon['alt']; ?>" 
        width="<?php echo $hero_image['sizes']['thumbnail-width']; ?>"
        height="<?php echo $hero_image['sizes']['thumbnail-height']; ?>" />
    </div>
  <?php } ?>

  <div class="header-footer button-group filters-product" id="filters">
    <span data-filter="all" class="is-checked"><?php esc_html_e( 'all', 'gulp-wordpress' ) ?></span>
    <?php  $product_parents_id = array(99, 86); ?>
    <?php $categories = get_categories( array( 'taxonomy' => 'product_cat', 'child_of' => 86 ) ); 
      foreach ($categories as $category) { ?>
        <span data-filter="<?php $var = strtolower($category->name); echo str_replace(' ', '', $var);?>"><?php echo $category->name; ?></span>
      <?php } ?>
  </div>
</section>

<?php
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

  // WP_Query arguments
  $args = array(
      'post_type' => 'product',
      'posts_per_page' => -1,
      'post__not_in' => array( 5445, 5446, 5697, 5590 )
  );

  // The Query
  $post_query = new WP_Query( $args );

  if ($post_query->have_posts()) : ?>
    <?php $count = 0; ?>
    <section class="container">

    <div class="column-3 grid product-list product-grid">
      <?php while($post_query->have_posts()) : $post_query->the_post();  ?>
      <?php $count++; ?>
      <?php
        $woo_product_cat = get_the_terms( $product->get_id(), 'product_cat' );
        $cat_product_name = $woo_product_cat[0]->name;
        $cat_stripped = str_replace(' ', '',  $cat_product_name); ?>

      <?php if($count == 7 ) { ?>
         <div class="grid-item product-grid-item banner-section section">
          <?php get_template_part('template-parts/promo', 'banner');?>
        </div>
      <?php } 
      if($count == 13 ) { ?>
        <div class="grid-item product-grid-item cta section">
          <?php get_template_part('template-parts/cta', 'contact');?>
        </div>
      <?php } ?>

      <div class="grid-item card-product product-grid-item <?php echo strtolower($cat_stripped); ?>" data-item="<?php echo strtolower($cat_stripped); ?>">
        <?php get_template_part('template-parts/loop', 'product'); ?>
      </div>

      <?php endwhile; ?>
      </div>
    </section>

    <?php 
    endif; 
    wp_reset_postdata();

    ?>

<!-- Add flexible content section  -->
<?php get_template_part('template-parts/flexible', 'contentsection');?>

<?php get_footer(); ?>