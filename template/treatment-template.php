<?php /* Template Name: Treatments */

get_header(); ?>

<?php // Treatment category  - get id 
  $treatment_parents_id = array(69, 63);
?>

<section class="container treatments-header">
  <div class="header-image">

    <?php $hero_image = get_field( 'hero_image' ); ?>
    <?php if ( $hero_image ) { ?>
      <img src="<?php echo $hero_image['sizes']['image_tablet']; ?>" 
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_image['alt']; ?>" 
        width="<?php echo $hero_image['sizes']['image_tablet-width']; ?>"
        height="<?php echo $hero_image['sizes']['image_tablet-height']; ?>"/>
    <?php }else if (has_post_thumbnail()){ ?>
      <?php $thumb_id = get_post_thumbnail_id(); ?>
      <img src="<?php $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
        echo $thumb_url = $thumb_url_array[0]; ?>" 
        srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
        echo $thumb_url_array_medium[0]; ?> 490w " sizes="(max-width: 1265px) 1265px, 100vw" 
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>"
        width="<?php echo $thumb_url_array[1]; ?>"
        height="<?php echo $thumb_url_array[2]; ?>" />
    <?php }?>
  </div>

  <div class="header-content">
    <?php the_field( 'hero_content' ); ?>
  </div>
  
  <?php $hero_mobile_icon = get_field( 'hero_mobile_icon' ); ?>
  <?php if ( $hero_mobile_icon ) { ?>
    <div class="treatments-icon">
      <img src="<?php echo $hero_mobile_icon['sizes']['thumbnail']; ?>" 
        alt="<?php _e('my-esthetic', 'myesthetic'); ?>-<?php echo $hero_mobile_icon['alt']; ?>" 
        width="<?php echo $hero_image['sizes']['thumbnail-width']; ?>"
        height="<?php echo $hero_image['sizes']['thumbnail-height']; ?>" />
    </div>
  <?php } ?>
  
  <div class="header-footer button-group filters-treatment" id="filters">
    <span data-filter="all" class="is-checked"><?php _e('All', 'myesthetic') ?></span>

    <?php $treatment_categories = get_categories( array( 'child_of' => $treatment_parents_id ) ); 
   
      foreach ($treatment_categories as $category) { ?>
        <span data-filter="<?php $var = strtolower($category->name); echo str_replace(' ', '', $var);?>"><?php echo $category->name; ?></span>
      <?php } ?>
  </div>
</section>
<?php // WP_Query arguments
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

  $args = array(
      'post_type' => 'treatment',
      'posts_per_page' => -1,
      'paged' => $paged
  );

  // The Query
  $post_query = new WP_Query( $args );

  if ($post_query->have_posts()) : ?>

    <?php $count = 0; ?>
    <section class="container treatments treatments-page">
      <div class="column-3 grid treatment-grid">

        <?php while ($post_query->have_posts()) : $post_query->the_post(); ?>
          <?php $count++; ?>
          <?php $categories = get_categories( array( 'child_of' => $treatment_parents_id ) ); ?>  
       
          <?php $categories = get_the_category();
           foreach ($categories as $category){
              $parent_id = $category->category_parent;
              if( $parent_id == 69 || $parent_id == 63) {
                $category_name = $category->name;
                $categoru_id = $category->term_id;
                $cat_stripped = str_replace(' ', '',  $category_name);
              }
            } ?>

          <?php if($count == 7 ) { ?>
              <div class="grid-item treatment-grid-item cta section" >
                <?php get_template_part('template-parts/cta', 'contact');?>
            </div>
          <?php } ?>

        <div class="card card-rating grid-item treatment-grid-item <?php echo strtolower($cat_stripped);  ?>" data-item="<?php echo strtolower($cat_stripped);  ?>">
          <?php get_template_part('template-parts/loop', 'treatment'); ?>
        </div>

        <?php endwhile; ?>
        </div>

        <?php
          echo "<div class='pagination'><div>" . paginate_links(array(
          'total' => $post_query->max_num_pages,
          'prev_text' => __('<img src="'.get_bloginfo('template_url').'/img/pagination-arrow-left.svg" width="31" height="21" alt="<" />'),
          'next_text' => __('<img src="'.get_bloginfo('template_url').'/img/pagination-arrow-right.svg" width="31" height="21" alt=">" />')
          )) . "</div></div>";
        ?>
    </section>

  <?php endif; 
  
  wp_reset_postdata(); ?>

<!-- Add flexible content section -->
<?php get_template_part('template-parts/flexible', 'contentsection');?>


<?php get_footer(); ?>