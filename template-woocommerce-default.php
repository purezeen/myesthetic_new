<?php
// Template name: WooCommerce page
 get_header(); ?>

<?php
	 $background = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); 
	 $backgroundImg = $background[0];
?>

<?php
if(has_post_thumbnail()) {
  
  $thumb_id = get_post_thumbnail_id();
  ?>
  <section class="single-blog-header container" >

    <img src="<?php 
      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', false);
      echo $thumb_url = $thumb_url_array[0]; ?>" 
      srcset="<?php $thumb_url_array_medium = wp_get_attachment_image_src($thumb_id, 'medium', false);
      echo $thumb_url_array_medium[0]; ?> 490w, 
      <?php $thumb_url_array_tablet = wp_get_attachment_image_src($thumb_id, 'image_tablet', false);
      echo $thumb_url_array_tablet[0]; ?> 800w, 
      <?php echo $thumb_url; ?> 1265w " 
      sizes="(max-width: 1265px) 1265px, 100vw" 
      alt="<?php _e('my-esthetic', 'myesthetic'); ?>" 
    />
  
  </section>
    <?php
    } 
    ?>

<main class="container">
  <div class="<?php if(!has_post_thumbnail()) { echo "remove-top"; }; ?>">
      <?php the_content(); ?>

      <?php get_template_part('template-parts/flexible', 'content')?>

  </div>
</main>

<div class="container cta cta-contact section">
 <?php get_template_part('template-parts/cta', 'contact');?>
</div>

<?php get_footer(); ?>