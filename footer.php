<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
		</div>
		<!-- #content -->

		<div class="scroll-to-top"> 
			<div class="scroll-to-top-mobile">
				<img src="<?php echo get_template_directory_uri() ?>/img/top-arrow.svg" alt="Toggle" width="53" height="150">
			</div>
			<div class="h6"><?php _e( 'Back to top', 'myesthetic' ); ?> <img src="<?php echo get_template_directory_uri() ?>/img/arrow-green.png" alt="Toggle" width="35" height="9"></div>
		</div>

		<div class="icon-bar">
		<div class="h6"><?php _e( 'Follow ', 'myesthetic' ); ?> - </div>
		<a href="<?php the_field( 'instagram_link', 'option' ); ?>" rel="noreferrer" target="_blank">Instagram</a>
		<a href="<?php the_field( 'facebook_link', 'option' ); ?>" rel="noreferrer" target="_blank">Facebook</a>
		<a href="<?php the_field( 'snap_link', 'option' ); ?>" rel="noreferrer" target="_blank">Snapchat</a>
		</div>

		<section class="page-footer">
			<a href="https://api.whatsapp.com/send?phone=16094776168" rel="noreferrer" target="_blank" class="social-icon">
				<img src="<?php echo get_template_directory_uri() ?>/img/whatsapp.svg" alt="Whatsapp" width="60" height="22">
				<span>whats up</span>
			</a>

			<a href="tel:<?php the_field( 'mobile_number', 'option' ); ?>" rel="noreferrer" class="social-icon">
				<img src="<?php echo get_template_directory_uri() ?>/img/call.svg" alt="Phome" width="60" height="22">
				<span><?php _e( 'phone', 'myesthetic' ); ?></span>
			</a>

			<a href="mailto:info@my-esthetic.de" rel="noreferrer" class="social-icon">
				<img src="<?php echo get_template_directory_uri() ?>/img/email.svg" alt="Email" width="60" height="22">
				<span>e-mail</span>
			</a>

			<a href="<?php the_field( 'instagram_link', 'option' ); ?>" rel="noreferrer" class="social-icon" target="_blank">
				<img src="<?php echo get_template_directory_uri() ?>/img/instagram.svg" alt="Instagram" width="60" height="22">
				<span>instagram</span>
			</a>
		</section>

		<footer class="container section footer">
			<div class="footer-top">
				<div class="footer-title">
					<h2><?php _e( 'How can we help?', 'myesthetic' ); ?></h2>
					<!-- <div class="h6"><?php //_e( 'Contact us anytime', 'myesthetic' ); ?></div> -->
					<a href="<?php _e( '/en/about-us', 'myesthetic' ); ?>/"
                      class="btn btn-full btn-green btn-arrow"><?php _e( 'Contact us', 'myesthetic' ); ?></a>
				</div>
				<ul class="footer-contact unstyle-list">
					<li>
						<span><?php _e( 'SEND US A MESSAGE:', 'myesthetic' ); ?></span>
						<a href="mailto:<?php the_field( 'email', 'option' ); ?>"><?php the_field( 'email', 'option' ); ?></a>
					</li>
					<li>
						<span><?php _e( 'CALL US:', 'myesthetic' ); ?></span>
						<a href="tel:<?php the_field( 'mobile_number', 'option' ); ?>"> <?php the_field( 'mobile_number', 'option' ); ?></a>
					</li>
				</ul>
			</div>
			<ul class="unstyle-list footer-bottom">
				<li class="footer-logo">
					<?php if ( get_field( 'logo', 'option' ) ) { ?>
						<!-- <a href="/"><img src="<?php //the_field( 'logo', 'option' ); ?>" alt="MyEsthetic" width="160" height="70"></a><br> -->
						<a href="/"><img src="<?php echo get_template_directory_uri() ?>/img/logo-footer.png" alt="MyEsthetic" width="160" height="70"></a><br>

					<?php } ?>
					<span class="copyright"><?php _e( 'All rights reserved', 'myesthetic' ); ?><br>
						Copyright, My Esthetic 2020</span>
				</li>
				<li>
					<ul>
						<li><strong><?php _e('Social', 'myesthetic'); ?></strong></li>
						<li><a href="<?php the_field( 'facebook_link', 'option' ); ?>">Facebook</a></li>
						<li><a href="<?php the_field( 'instagram_link', 'option' ); ?>">Instagram</a></li>
						<li><a href="<?php the_field( 'snap_link', 'option' ); ?>">Snapchat</a></li>
						<li><a href="<?php the_field( 'linkedin_link', 'option' ); ?>">LinkedIn</a></li>
						<!-- <li><a href="<?php the_field( 'youtube_link', 'option' ); ?>">Youtube</a></li> -->
					</ul>
				</li>
				<li>
					<strong><?php _e('Links', 'myesthetic'); ?></strong>
					<?php wp_nav_menu( array( 'theme_location' => 'footer_links' ) ); ?>
				</li>
				<li>
					<strong><?php _e('Offer', 'myesthetic'); ?></strong>
					<?php wp_nav_menu( array( 'theme_location' => 'footer_treatments' ) ); ?>
				</li>
			</ul>
		</footer>
		</div>
		<!-- #page -->

		<!-- Facebook Pixel Code -->
		<script>
			jQuery(document).ready(function() {
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=true;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window,document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '1112328932157600'); 
				fbq('track', 'PageView');
			});
		</script>
		<noscript>
			<img height="1" width="1" src="https://www.facebook.com/tr?id=1112328932157600&ev=PageView&noscript=1" alt="Facebook"/>
		</noscript>
		<!-- End Facebook Pixel Code -->

		<?php wp_footer(); ?>
	</body>
</html>